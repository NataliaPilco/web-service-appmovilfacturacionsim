package atrums.util;

import java.io.*;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Configuracion {
	static final Logger log = Logger.getLogger(Configuracion.class);
	//private InputStream confService = null;
	//Properties propiedad = new Properties();
	
	//conexion a .sh
	Properties prop = new Properties();
	private InputStream confS = null;
	
	private String ipsevidor;
	private String puertoservidor;
	private String usuarioservidor;
	private String passwordservidor;
	private String bddsevidor;
	
	public Configuracion(String path) {
		try {
			//this.confService = this.getClass().getClassLoader().getResourceAsStream("./..//Configuracion/configuracion.xml");
			//this.propiedad.loadFromXML(confService);
	
			this.confS = new FileInputStream("/opt/service-conexion-app/"+path);
			//this.confS = new FileInputStream("C:\\NPG\\service-conexion-app\\"+path);
			this.prop.load(confS);
			this.ipsevidor = this.prop.getProperty("ipservidor");
			this.puertoservidor = this.prop.getProperty("puertoservidor");
			this.usuarioservidor = this.prop.getProperty("usuariosservidor");
			this.passwordservidor = this.prop.getProperty("passwordservidor");
			this.bddsevidor = this.prop.getProperty("bddservidor");
			
			//this.ipsevidor = this.propiedad.getProperty("ipservidor");
			//this.puertoservidor = this.propiedad.getProperty("puertoservidor");
			//this.usuarioservidor = this.propiedad.getProperty("usuariosservidor");
			//this.passwordservidor = this.propiedad.getProperty("passwordservidor");
			//this.bddsevidor = this.propiedad.getProperty("bddservidor");
			
			this.prop.clear();
			this.confS.close();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	/*public Properties getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(Properties propiedad) {
		this.propiedad = propiedad;
	}*/

	public String getIpsevidor() {
		return ipsevidor;
	}

	public void setIpsevidor(String ipsevidor) {
		this.ipsevidor = ipsevidor;
	}

	public String getPuertoservidor() {
		return puertoservidor;
	}

	public void setPuertoservidor(String puertoservidor) {
		this.puertoservidor = puertoservidor;
	}

	public String getUsuarioservidor() {
		return usuarioservidor;
	}

	public void setUsuarioservidor(String usuarioservidor) {
		this.usuarioservidor = usuarioservidor;
	}

	public String getPasswordservidor() {
		return passwordservidor;
	}

	public void setPasswordservidor(String passwordservidor) {
		this.passwordservidor = passwordservidor;
	}

	public String getBddsevidor() {
		return bddsevidor;
	}

	public void setBddsevidor(String bddsevidor) {
		this.bddsevidor = bddsevidor;
	}
	
	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}
}
