package atrums.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import atrums.modelo.CategoriaImpuesto;
import atrums.modelo.Factura;
import atrums.modelo.Impuesto;
import atrums.modelo.LineasFactura;
import atrums.modelo.Lista;
import atrums.modelo.MetodoPago;
import atrums.modelo.Producto;
import atrums.modelo.Tercero;
import atrums.persistencia.Conexion;
import atrums.persistencia.OperacionesBDD;
import atrums.persistencia.Validaciones;

import javax.sql.DataSource;

@Path("/")
public class Operaciones {
	static final Logger log = Logger.getLogger(Operaciones.class);

	/*
	 * private Conexion dataconexion = new Conexion(pathConexion); private
	 * DataSource dataSourceOpenbravo = null;
	 * 
	 * public Operaciones() { if (this.dataSourceOpenbravo == null) {
	 * this.dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo(); } }
	 */

	@POST
	@Path("ingresoapp")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String loginAppPostJson(String inputJsonObj) {
		String user = "";
		String password = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {

			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			user = (String) auxInputJsonObj.get("user");
			password = (String) auxInputJsonObj.get("password");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = login(user, password, path);

		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listClientesAtrums")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listClientesAtrumsJson() {

		JSONObject auxmensaje = new JSONObject();

		try {

			auxmensaje = listarClientesAtrumsJson();

		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("solicitarinformacion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarPedidoPutJson(String inputJsonObj) {
		String nombre = "";
		String identificacion = "";
		String empresa = "";
		String telefono = "";
		String correo = "";
		String comentario_cliente = "";
		String path = "conec-facturacion.sh";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			nombre = (String) auxInputJsonObj.get("nombre");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			empresa = (String) auxInputJsonObj.get("empresa");
			telefono = (String) auxInputJsonObj.get("telefono");
			correo = (String) auxInputJsonObj.get("correo");
			comentario_cliente = (String) auxInputJsonObj.get("comentario_cliente");

			auxmensaje = registrarSolicitud(nombre, identificacion, empresa, telefono, correo, comentario_cliente,
					path);
			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("insertpartner")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String savePartnerPutJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String tipo_persona = "";
		String tipo_identificacion = "";
		String identificacion = "";
		String nombre = "";
		String apellido = "";
		String nombre_comercial = "";
		String razon_social = "";
		String email = "";
		String telefono = "";
		String direccion = "";
		int control = 1; // va 1 porque se va a registrar un tercero desde la ventana cliente
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			tipo_persona = (String) auxInputJsonObj.get("tipo_persona");
			tipo_identificacion = (String) auxInputJsonObj.get("tipo_identificacion");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			nombre = (String) auxInputJsonObj.get("nombre");
			apellido = (String) auxInputJsonObj.get("apellido");
			nombre_comercial = (String) auxInputJsonObj.get("nombre_comercial");
			razon_social = (String) auxInputJsonObj.get("razon_social");
			email = (String) auxInputJsonObj.get("email");
			telefono = (String) auxInputJsonObj.get("telefono");
			direccion = (String) auxInputJsonObj.get("direccion");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = insertPartner(idusuario, idcliente, identificacion, nombre_comercial, razon_social,
					tipo_identificacion, email, nombre, apellido, tipo_persona, telefono, direccion, control, path);
			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("validatepartner")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String validatePartnerPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String tipo_persona = "";
		String tipo_identificacion = "";
		String identificacion = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			tipo_persona = (String) auxInputJsonObj.get("tipo_persona");
			tipo_identificacion = (String) auxInputJsonObj.get("tipo_identificacion");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = buscarTerceroJson(idusuario, idcliente, tipo_persona, tipo_identificacion, identificacion,
					path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("searchlistproducts")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String searchListProductsPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = searchListProductJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listtax")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listTaxPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listarImpuestoJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}
	
	@POST
	@Path("listpaymentmethod")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listPaymentMethodPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listarPaymentMethodJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("insertinvoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String insertInvoicePutJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String idsucursal = "";
		String nombre_tipo_doc = "";
		String idmetodo_pago = "";
		String iddireccion_tercero = "";
		String identificacion = "";
		String nombre_comercial = "";
		String razon_social = "";
		String tipo_identificacion = "";
		String email = "";
		String nombre = "";
		String apellido = "";
		String tipo_persona = "";
		String telefono = "";
		String direccion = "";
		String idtercero = "";
		String idproducto = "";
		double cantidad = 0;
		double precio = 0;
		double val_tot_linea = 0;
		String idimpuesto = "";
		String idfactura = "";
		String idmodfactura = "";
		int tipo = 0; // 1 factura de venta, 2 nota de credito
		int control_tercero = 2; // va 2 porque se va a registrar un tercero desde la ventana factura
		int control_ldm = 0;
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			idsucursal = (String) auxInputJsonObj.get("idsucursal");
			nombre_tipo_doc = (String) auxInputJsonObj.get("nombre_tipo_doc");
			idmetodo_pago = (String) auxInputJsonObj.get("idmetodo_pago");
			iddireccion_tercero = (String) auxInputJsonObj.get("iddireccion_tercero");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			nombre_comercial = (String) auxInputJsonObj.get("nombre_comercial");
			razon_social = (String) auxInputJsonObj.get("razon_social");
			tipo_identificacion = (String) auxInputJsonObj.get("tipo_identificacion");
			email = (String) auxInputJsonObj.get("email");
			nombre = (String) auxInputJsonObj.get("nombre");
			apellido = (String) auxInputJsonObj.get("apellido");
			tipo_persona = (String) auxInputJsonObj.get("tipo_persona");
			telefono = (String) auxInputJsonObj.get("telefono");
			direccion = (String) auxInputJsonObj.get("direccion");
			idtercero = (String) auxInputJsonObj.get("idtercero");
			idproducto = (String) auxInputJsonObj.get("idproducto");
			cantidad = (Double) auxInputJsonObj.getDouble("cantidad");
			precio = (Double) auxInputJsonObj.getDouble("precio");
			val_tot_linea = (Double) auxInputJsonObj.getDouble("val_tot_linea");
			idimpuesto = (String) auxInputJsonObj.get("idimpuesto");
			idfactura = (String) auxInputJsonObj.get("idfactura");
			idmodfactura = (String) auxInputJsonObj.get("idmodfactura");
			tipo = (Integer) auxInputJsonObj.getInt("tipo");
			control_ldm = (Integer) auxInputJsonObj.getInt("control_ldm");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = insertinvoice(idusuario, idcliente, idsucursal, nombre_tipo_doc, idmetodo_pago, iddireccion_tercero,
					identificacion, nombre_comercial, razon_social, tipo_identificacion, email, nombre, apellido,
					tipo_persona, telefono, direccion, idtercero, idproducto, cantidad, precio, val_tot_linea,
					idimpuesto, idfactura, idmodfactura, tipo, control_ldm, control_tercero, path);

			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("searchdatainvoice")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String searchDataInvoicePostJson(String inputJsonObj) {
		String idusuario = "";
		String idfactura = "";
		String path = "";

		// String mensaje = "<?xml version=\"1.0\"?>";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idfactura = (String) auxInputJsonObj.get("idfactura");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = searchDataInvoiceJson(idusuario, idfactura, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listarlineasfactura")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listarLineasFacturaPostJson(String inputJsonObj) {
		String idusuario = "";
		String idfactura = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idfactura = (String) auxInputJsonObj.get("idfactura");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listarLineasFacturaJson(idusuario, idfactura, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("deleteinvoiceline")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteInvoiceLinePutJson(String inputJsonObj) {
		String idusuario = "";
		String idline = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idline = (String) auxInputJsonObj.get("idline");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = deleteInvoiceLine(idusuario, idline, path);

			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("selectlineafactura")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String selectLineaFacturaPostJson(String inputJsonObj) {
		String idusuario = "";
		String idline = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idline = (String) auxInputJsonObj.get("idline");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = selectInvoiceLine(idusuario, idline, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("updateinvoiceline")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateInvoiceLinePutJson(String inputJsonObj) {
		String idusuario = "";
		String idline = "";
		String idproducto = "";
		String idimpuesto = "";
		double cantidad = 0;
		double precio = 0;
		double val_tot_linea = 0;
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idline = (String) auxInputJsonObj.get("idline");
			idproducto = (String) auxInputJsonObj.get("idproducto");
			idimpuesto = (String) auxInputJsonObj.get("idimpuesto");
			cantidad = (Double) auxInputJsonObj.getDouble("cantidad");
			precio = (Double) auxInputJsonObj.getDouble("precio");
			val_tot_linea = (Double) auxInputJsonObj.getDouble("val_tot_linea");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = updateInvoiceLine(idusuario, idline, idproducto, idimpuesto, cantidad, precio, val_tot_linea,
					path);

			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("processinvoice")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String processInvoicePutJson(String inputJsonObj) {
		String idusuario = "";
		String idfactura = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idfactura = (String) auxInputJsonObj.get("idfactura");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = processInvoice(idusuario, idfactura, path);

			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("selectinvoice")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String selectInvoicePostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String idsucursal = "";
		Integer tipo = 0; // 1 factura, 2 nota de credito
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			idsucursal = (String) auxInputJsonObj.get("idsucursal");
			tipo = (Integer) auxInputJsonObj.getInt("tipo");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = selectInvoice(idusuario, idcliente, idsucursal, tipo, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@PUT
	@Path("insertproduct")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String saveProductPutJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String identificador = "";
		String nombre = "";
		String descripcion = "";
		String idimpuesto = "";
		String tipo = "";
		double precio = 0;
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			identificador = (String) auxInputJsonObj.get("identificador");
			nombre = (String) auxInputJsonObj.get("nombre");
			descripcion = (String) auxInputJsonObj.get("descripcion");
			idimpuesto = (String) auxInputJsonObj.get("idimpuesto");
			tipo = (String) auxInputJsonObj.get("tipo");
			precio = (Double) auxInputJsonObj.getDouble("precio");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = insertProduct(idusuario, idcliente, identificador, nombre, descripcion, idimpuesto, tipo,
					precio, path);

			auxmensaje.put("type", true);
		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {
				auxmensaje.put("warn", ex.getMessage());
				auxmensaje.put("type", false);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listcategorytax")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listCategoryTaxPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listarCategoriaImpuestoJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listpartners")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listPartnersPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listPartnerJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("listproducts")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String listProductsPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = listProductJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("selectinvoicenc")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String selectInvoiceNCPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String idsucursal = "";
		String idtercero = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			idsucursal = (String) auxInputJsonObj.get("idsucursal");
			idtercero = (String) auxInputJsonObj.get("idtercero");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = selectInvoiceNC(idusuario, idcliente, idsucursal, idtercero, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("selectinvoicencmod")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String selectInvoiceModNCPostJson(String inputJsonObj) {
		String idusuario = "";
		String idfactura = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idfactura = (String) auxInputJsonObj.get("idfactura");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = searchDataInvoiceModJson(idusuario, idfactura, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("closesession")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String closeSessionPostJson(String inputJsonObj) {
		String idusuario = "";
		String idsession = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idsession = (String) auxInputJsonObj.get("idsession");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = closeSession(idusuario, idsession, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("verificarsession")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String checkSessionPostJson(String inputJsonObj) {
		String idusuario = "";
		String idsession = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idsession = (String) auxInputJsonObj.get("idsession");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = verificarSession(idusuario, idsession, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("resetsession")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String resetSessionPostJson(String inputJsonObj) {
		String user = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {

			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			user = (String) auxInputJsonObj.get("user");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = resetSession(user, path);

		} catch (JSONException ex) {

			log.warn(ex.getMessage());

			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("validatephone")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String validatePhonePostJson(String inputJsonObj) {
		String idusuario = "";
		String telefono = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			telefono = (String) auxInputJsonObj.get("telefono");

			auxmensaje = validatePhone(idusuario, telefono);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	@POST
	@Path("validateemail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String validateEmailPostJson(String inputJsonObj) {
		String idusuario = "";
		String email = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			email = (String) auxInputJsonObj.get("email");

			auxmensaje = validateEmail(idusuario, email);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}
	
	@POST
	@Path("searchproductldm")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String searchProductsLdmPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			path = (String) auxInputJsonObj.get("path");

			auxmensaje = searchProductLdmJson(idusuario, idcliente, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}
	
	@POST
	@Path("searchpartnertext")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String buscarTercerosTextoPostJson(String inputJsonObj) {
		String idusuario = "";
		String idcliente = "";
		String dato = "";
		String path = "";

		JSONObject auxmensaje = new JSONObject();

		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);

			idusuario = (String) auxInputJsonObj.get("idusuario");
			idcliente = (String) auxInputJsonObj.get("idcliente");
			dato = (String) auxInputJsonObj.get("dato");
			path = (String) auxInputJsonObj.get("path");	

			auxmensaje = buscarTercerosTextoJson(idusuario, idcliente, dato, path);

		} catch (JSONException ex) {
			log.warn(ex.getMessage());
			try {

				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return auxmensaje.toString();
	}

	/***************************************************************************************************************
	 ***************************************************************************************************************
	 ***************************************************************************************************************
	 ***************************************************************************************************************
	 ***************************************************************************************************************/

	private JSONObject login(String usuario, String password, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (usuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else if (password.equals("")) {
				auxSeguridad.put("password", "No hay password");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", usuario);
			}

			auxRespuesta.put("usuario", usuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {
						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						int conexion = 0;
						String idusuario = "", idcliente = "", idsucursal = "", idsession = "";

						conexion = home.loggin(usuario, password, conHome);

						if (conexion == 2) {
							idusuario = home.idUser(usuario, password, conHome);
							idcliente = home.idClient(idusuario, conHome);
							idsucursal = home.buscarSucursal(idcliente, "Factura Electr�nica", conHome);
							idsession = home.checkSession(idusuario, conHome);

							if (idusuario == "" || idcliente == "" || idsucursal == "" || idsession == "") {
								auxRespuesta.put("idusuario", "N/A");
								auxRespuesta.put("idcliente", "N/A");
								auxRespuesta.put("idsucursal", "N/A");
								auxRespuesta.put("idsession", "N/A");
								auxRespuesta.put("verificacion", 0);
								auxRespuesta.put("mensaje", "Error, no se encontraron los datos de acceso");
							} else {
								auxRespuesta.put("idusuario", idusuario);
								auxRespuesta.put("idcliente", idcliente);
								auxRespuesta.put("idsucursal", idsucursal);
								auxRespuesta.put("idsession", idsession);
								auxRespuesta.put("verificacion", conexion);
								auxRespuesta.put("mensaje", "Conexi�n exitosa");
							}

						} else {
							auxRespuesta.put("idusuario", "N/A");
							auxRespuesta.put("idcliente", "N/A");
							auxRespuesta.put("idsucursal", "N/A");
							auxRespuesta.put("idsession", "N/A");
							auxRespuesta.put("verificacion", conexion);
							if (conexion == 1) {
								auxRespuesta.put("mensaje", "Dispositivo conectado");
							} else if (conexion == 0) {
								auxRespuesta.put("mensaje", "Su usuario y/o contrase�a son incorrectas");
							}
						}

					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Las credenciales son incorrectas");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listarClientesAtrumsJson() {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();

		try {

			if (continuar) {

				try {
					ArrayList<Lista> datosLista = new ArrayList<Lista>();

					OperacionesBDD home = new OperacionesBDD();
					datosLista = home.listaClientesAtrums("/opt/service-conexion-app/lista.sh");
					//datosLista = home.listaClientesAtrums("C:\\NPG\\service-conexion-app\\lista.sh");

					// poner la parte para convertir a json

					List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

					Map<String, Object> map;

					JSONArray jsArray = new JSONArray();

					for (int i = 0; i < datosLista.size(); i++) {
						map = new HashMap<String, Object>();
						map.put("archivo", datosLista.get(i).getArchivo());
						map.put("nombre", datosLista.get(i).getNombre());

						list.add(map);
						jsArray.put(map);
					}

					auxRespuesta.put("lista", jsArray);

				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
					} catch (Exception ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Error, comun�quese con el Administrador");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject registrarSolicitud(String nombre, String identificacion, String empresa, String telefono,
			String correo, String comentario_cliente, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (telefono.equals("")) {
				auxSeguridad.put("Telefono", "No registra telefono");
				continuar = false;
			} else {
				auxSeguridad.put("Telefono", telefono);
			}

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String ingresarSolicitud = home.registrarSolicitud(nombre, identificacion, empresa, telefono,
								correo, comentario_cliente, conHome);

						// poner la parte para convertir a json
						auxRespuesta.put("resultado", ingresarSolicitud);

					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Error al registrar");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject insertPartner(String idusuario, String idcliente, String identificacion, String nombre_comercial,
			String razon_social, String tipo_identificacion, String email, String nombre, String apellido,
			String tipo_persona, String telefono, String direccion, int control, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String ingresarTercero = home.insertarTercero(idusuario, idcliente, identificacion,
								nombre_comercial, razon_social, tipo_identificacion, email, nombre, apellido,
								tipo_persona, telefono, direccion, control, conHome);

						// poner la parte para convertir a json
						auxRespuesta.put("resultado", ingresarTercero);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject buscarTerceroJson(String idusuario, String idcliente, String tipo_persona,
			String tipo_identificacion, String identificacion, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Tercero> datosTercero = new ArrayList<Tercero>();

						datosTercero = home.obtenerTercero(idcliente, identificacion, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosTercero.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idtercero", datosTercero.get(i).getIdTercero());
							map.put("idcliente", datosTercero.get(i).getIdCliente());
							map.put("identificacion", datosTercero.get(i).getIdentificacion());
							map.put("nombrecomercial", datosTercero.get(i).getNombreComercial());
							map.put("razonsocial", datosTercero.get(i).getRazonSocial());
							map.put("tipoidentificacion", datosTercero.get(i).getTipoIdentificacion());
							map.put("email", datosTercero.get(i).getEmail());
							map.put("nombre", datosTercero.get(i).getNombre());
							map.put("apellido", datosTercero.get(i).getApellido());
							map.put("razonsocial", datosTercero.get(i).getRazonSocial());
							map.put("tipopersona", datosTercero.get(i).getTipoPersona());
							map.put("telefono", datosTercero.get(i).getTelefono());
							map.put("direccion", datosTercero.get(i).getDireccion());
							map.put("idusuario", datosTercero.get(i).getIdUsuario());
							map.put("iddirectercero", datosTercero.get(i).getIdDirecTercero());
							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("tercero", jsArray);

						if (datosTercero.size() > 0) {
							auxRespuesta.put("mensaje",
									"El n�mero de identificaci�n " + identificacion + " ya se encuentra registrado");
						} else {
							// validacion de la identificacion
							Validaciones val = new Validaciones();
							String validate = val.validarIdentificacion(tipo_persona, tipo_identificacion,
									identificacion);
							auxRespuesta.put("mensaje", validate);
						}
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listPartnerJson(String idusuario, String idcliente, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Tercero> datosTercero = new ArrayList<Tercero>();

						datosTercero = home.obtenerListaTerceros(idcliente, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosTercero.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idtercero", datosTercero.get(i).getIdTercero());
							map.put("nombrecomercial", datosTercero.get(i).getNombreComercial());
							map.put("tipoidentificacion", datosTercero.get(i).getTipoIdentificacion());
							map.put("tipopersona", datosTercero.get(i).getTipoPersona());
							map.put("identificacion", datosTercero.get(i).getIdentificacion());
							map.put("telefono", datosTercero.get(i).getTelefono());
							map.put("email", datosTercero.get(i).getEmail());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("tercero", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject searchListProductJson(String idusuario, String idcliente, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String tarifa = "1.VENTA";

						ArrayList<Producto> datosProducto = new ArrayList<Producto>();

						datosProducto = home.obtenerListaProductos(idcliente, tarifa, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosProducto.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idproducto", datosProducto.get(i).getIdProducto());
							map.put("nombre", datosProducto.get(i).getNombre());
							map.put("precio", datosProducto.get(i).getPrecio());
							map.put("idcategoria", datosProducto.get(i).getIdCategoriaIva());
							map.put("nombrecategoria", datosProducto.get(i).getNombreCategoriaIva());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("producto", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listarImpuestoJson(String idusuario, String idcliente, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Impuesto> datosImpuesto = new ArrayList<Impuesto>();

						datosImpuesto = home.obtenerListaImpuesto(idcliente, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosImpuesto.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idimpuesto", datosImpuesto.get(i).getIdImpuesto());
							map.put("nombre", datosImpuesto.get(i).getNombre());
							map.put("idcategoria", datosImpuesto.get(i).getIdCategoriaIva());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("impuesto", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}
	
	private JSONObject listarPaymentMethodJson(String idusuario, String idcliente, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<MetodoPago> datosMetodo = new ArrayList<MetodoPago>();

						datosMetodo = home.obtenerListaMetodoPago(idcliente, conHome);  

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosMetodo.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idmetodo_pago", datosMetodo.get(i).getIdMetodoPago());
							map.put("nombre", datosMetodo.get(i).getNombre());
							map.put("valor_defecto", datosMetodo.get(i).getValorDefecto());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("metodopago", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject insertinvoice(String idusuario, String idcliente, String idsucursal, String nombre_tipo_doc, String idmetodo_pago,
			String iddireccion_tercero, String identificacion, String nombre_comercial, String razon_social,
			String tipo_identificacion, String email, String nombre, String apellido, String tipo_persona,
			String telefono, String direccion, String idtercero, String idproducto, double cantidad, double precio,
			double valor_total, String idimpuesto, String idfactura, String idmodfactura, int tipo, int control_ldm, 
			int control_tercero, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {
						// Buscar Cliente
						String iddoctype = home.buscarTipoDocumento(idcliente, nombre_tipo_doc, conHome);

						String idcondicion_pago = home.buscarCondicionPago(idcliente, "CONTADO", conHome);
						String idtarifa = home.buscarTarifaCV(idcliente, "1.VENTA", conHome);
						//String idmetodo_pago = home.buscarMetodoPago(idcliente, "EFECTIVO", conHome);

						if (iddoctype != "" && idcondicion_pago != "" && idtarifa != "") {

							String insertarLinea = null;
							insertarLinea = home.insertarInvoiceLine(idusuario, idcliente, iddoctype,
									iddireccion_tercero, identificacion, nombre_comercial, razon_social,
									tipo_identificacion, email, nombre, apellido, tipo_persona, telefono, direccion,
									idsucursal, idtercero, idproducto, cantidad, precio, valor_total, idimpuesto,
									idfactura, idmodfactura, idcondicion_pago, idtarifa, idmetodo_pago, tipo, control_ldm,
									control_tercero, conHome);

							// poner la parte para convertir a json
							// verificar registro de la factura
							String invoice = null;
							invoice = home.verificarCreacionFactura(insertarLinea, conHome); // el insertarLinea,
																								// devuelve
																								// el idfactura si lo
																								// registro
							if (invoice != null) {
								auxRespuesta.put("factura", invoice);
							} else {
								auxRespuesta.put("factura", "null");
							}
							auxRespuesta.put("resultado", insertarLinea);

						} else {
							auxRespuesta.put("factura", "null");
							auxRespuesta.put("resultado", "No se encontraron los datos para facturar, tipo de documento, condici�n de pago, tarifa o m�todo de pago");
						}

					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject searchDataInvoiceJson(String idusuario, String idfactura, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
					} else {

						ArrayList<Factura> factura = new ArrayList<Factura>();

						factura = home.obtenerInforFactura(idfactura, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < factura.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfactura", factura.get(i).getIdFactura());
							map.put("tipodocumento", factura.get(i).getTipoDocumento());
							map.put("nrodocumento", factura.get(i).getNroDocumento());
							map.put("puntoemision", factura.get(i).getPuntoEmision());
							map.put("nroestablecimiento", factura.get(i).getNroEstablecimiento());
							map.put("estado", factura.get(i).getEstado());
							map.put("idtercero", factura.get(i).getIdTercero());
							map.put("idcliente", factura.get(i).getIdCliente());
							map.put("identificacion", factura.get(i).getIdentificacion());
							map.put("nombrecomercial", factura.get(i).getNombreComercial());
							map.put("razonsocial", factura.get(i).getRazonSocial());
							map.put("tipoidentificacion", factura.get(i).getTipoIdentificacion());
							map.put("email", factura.get(i).getEmail());
							map.put("nombre", factura.get(i).getNombre());
							map.put("apellido", factura.get(i).getApellido());
							map.put("razonsocial", factura.get(i).getRazonSocial());
							map.put("tipopersona", factura.get(i).getTipoPersona());
							map.put("telefono", factura.get(i).getTelefono());
							map.put("direccion", factura.get(i).getDireccion());
							map.put("idusuario", factura.get(i).getIdUsuario());
							map.put("iddirectercero", factura.get(i).getIdDirecTercero());
							map.put("idmodfactura", factura.get(i).getIdModFactura());
							map.put("nromoddocumento", factura.get(i).getNroModDocumento());
							map.put("c_subtotal", factura.get(i).getSubTotal());
							map.put("c_total", factura.get(i).getTotal());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("factura", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listarLineasFacturaJson(String idusuario, String idfactura, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<LineasFactura> datosLineas = new ArrayList<LineasFactura>();

						datosLineas = home.listarLineasFactura(idfactura, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosLineas.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfacturaline", datosLineas.get(i).getIdFacturaLine());
							map.put("idproducto", datosLineas.get(i).getIdProducto());
							map.put("nombre", datosLineas.get(i).getNombre());
							map.put("cantidad", datosLineas.get(i).getCantidad());
							map.put("preciounitario", datosLineas.get(i).getPrecioUnitario());
							map.put("valortotal", datosLineas.get(i).getValorTotal());
							map.put("impuesto", datosLineas.get(i).getImpuesto());
							map.put("valorimpuesto", datosLineas.get(i).getValorImpuesto());
							map.put("idfactura", datosLineas.get(i).getIdFactura());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("lista", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject deleteInvoiceLine(String idusuario, String idLine, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String eliminarLinea = null;

						eliminarLinea = home.deleteInvoiceLine(idLine, conHome);

						auxRespuesta.put("resultado", eliminarLinea);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject selectInvoiceLine(String idusuario, String idline, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<LineasFactura> datosLinea = new ArrayList<LineasFactura>();

						datosLinea = home.selectInvoiceLine(idline, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosLinea.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfacturaline", datosLinea.get(i).getIdFacturaLine());
							map.put("idproducto", datosLinea.get(i).getIdProducto());
							map.put("nombre", datosLinea.get(i).getNombre());
							map.put("cantidad", datosLinea.get(i).getCantidad());
							map.put("preciounitario", datosLinea.get(i).getPrecioUnitario());
							map.put("valortotal", datosLinea.get(i).getValorTotal());
							map.put("idimpuesto", datosLinea.get(i).getIdImpuesto());
							map.put("impuesto", datosLinea.get(i).getImpuesto());
							map.put("valorimpuesto", datosLinea.get(i).getValorImpuesto());
							map.put("idfactura", datosLinea.get(i).getIdFactura());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("linea", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject updateInvoiceLine(String idusuario, String idline, String idproducto, String idimpuesto,
			double cantidad, double precio, double valor_total, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String eliminarLinea = null;

						eliminarLinea = home.updateInvoiceLine(idline, idproducto, idimpuesto, cantidad, precio,
								valor_total, conHome);

						auxRespuesta.put("resultado", eliminarLinea);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject processInvoice(String idusuario, String idfactura, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {
						String procesarFactura = null;
						Boolean line = home.existInvoiceLine(idfactura, conHome);

						if (line == true) {
							procesarFactura = home.processInvoice(idfactura, conHome);
							auxRespuesta.put("resultado", procesarFactura);
							auxRespuesta.put("line", line);
						} else {
							auxRespuesta.put("resultado", "La factura no tiene l�neas registradas");
							auxRespuesta.put("line", line);
						}
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject selectInvoice(String idusuario, String idcliente, String idsucursal, int tipo, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Factura> datosfactura = new ArrayList<Factura>();

						datosfactura = home.listInvoice(idcliente, idsucursal, tipo, conHome);

						// poner la parte para convertir a json
						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosfactura.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfactura", datosfactura.get(i).getIdFactura());
							map.put("nrodocumento", datosfactura.get(i).getNroDocumento());
							map.put("estadosri", datosfactura.get(i).getEstadoSRI());
							map.put("fechafactura", datosfactura.get(i).getFechaFactura());
							map.put("nombrecomercial", datosfactura.get(i).getNombreComercial());
							map.put("identificacion", datosfactura.get(i).getIdentificacion());
							map.put("subtotal", datosfactura.get(i).getSubTotal());
							map.put("total", datosfactura.get(i).getTotal());
							map.put("idmodfactura", datosfactura.get(i).getIdModFactura());
							map.put("nromoddocumento", datosfactura.get(i).getNroModDocumento());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("factura", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listarCategoriaImpuestoJson(String idusuario, String idcliente, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<CategoriaImpuesto> datosImpuesto = new ArrayList<CategoriaImpuesto>();

						datosImpuesto = home.obtenerListaCategoriaImpuesto(idcliente, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosImpuesto.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idimpuesto", datosImpuesto.get(i).getIdCategoriaImpuesto());
							map.put("nombre", datosImpuesto.get(i).getNombre());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("impuesto", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject insertProduct(String idusuario, String idcliente, String identificador, String nombre,
			String descripcion, String idimpuesto, String tipo, double precio, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {
						
						String tarifav = "1.VENTA";

						String ingresarProducto = home.insertProduct(idusuario, idcliente, identificador, nombre,
								descripcion, idimpuesto, tipo, precio, tarifav, conHome);

						// poner la parte para convertir a json
						auxRespuesta.put("resultado", ingresarProducto);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject listProductJson(String idusuario, String idcliente, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String tarifa = "1.VENTA";
						ArrayList<Producto> datosProducto = new ArrayList<Producto>();

						datosProducto = home.listaProductos(idcliente, tarifa, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosProducto.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idproducto", datosProducto.get(i).getIdProducto());
							map.put("identificador", datosProducto.get(i).getIdentificador());
							map.put("fechacreacion", datosProducto.get(i).getFechaCreacion());
							map.put("nombre", datosProducto.get(i).getNombre());
							map.put("precio", datosProducto.get(i).getPrecio());
							map.put("idcategoria", datosProducto.get(i).getIdCategoriaIva());
							map.put("nombrecategoria", datosProducto.get(i).getNombreCategoriaIva());
							map.put("stock", datosProducto.get(i).getStock());
							map.put("tipo", datosProducto.get(i).getTipo());
							map.put("precio", datosProducto.get(i).getPrecio());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("producto", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject selectInvoiceNC(String idusuario, String idcliente, String idsucursal, String idTercero,
			String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Factura> datosfactura = new ArrayList<Factura>();

						datosfactura = home.listInvoiceNC(idcliente, idsucursal, idTercero, conHome);

						// poner la parte para convertir a json
						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosfactura.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfactura", datosfactura.get(i).getIdFactura());
							map.put("nrodocumento", datosfactura.get(i).getNroDocumento());
							map.put("c_total", datosfactura.get(i).getTotal());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("facturanc", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject searchDataInvoiceModJson(String idusuario, String idfactura, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Factura> datosfactura = new ArrayList<Factura>();

						datosfactura = home.obtenerDocModNC(idfactura, conHome);

						// poner la parte para convertir a json
						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosfactura.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idfactura", datosfactura.get(i).getIdFactura());
							map.put("nrodocumento", datosfactura.get(i).getNroDocumento());
							map.put("c_total", datosfactura.get(i).getTotal());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("facturanc", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject closeSession(String idusuario, String idsession, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						boolean session = false;

						session = home.closeSession(idusuario, idsession, conHome);

						auxRespuesta.put("resultado", session);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject resetSession(String usuario, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (usuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", usuario);
			}

			auxRespuesta.put("usuario", usuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {
						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						boolean session = false;

						session = home.resetSession(usuario, conHome);

						auxRespuesta.put("resultado", session);

					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Las credenciales son incorrectas");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject verificarSession(String idusuario, String idsession, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						boolean session = false;

						session = home.verificarSession(idsession, conHome);

						auxRespuesta.put("resultado", session);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject validatePhone(String idusuario, String telefono) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {

				try {
					Validaciones val = new Validaciones();
					String validate = val.validarTelefono(telefono);

					if (validate == "") {
						auxRespuesta.put("verificacion", true);
					} else {
						auxRespuesta.put("verificacion", false);
					}

					auxRespuesta.put("mensaje", validate);
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
					} catch (Exception ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}

	private JSONObject validateEmail(String idusuario, String email) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {

				try {
					Validaciones val = new Validaciones();
					String validate = val.validarEmail(email);

					if (validate == "") {
						auxRespuesta.put("verificacion", true);
					} else {
						auxRespuesta.put("verificacion", false);
					}

					auxRespuesta.put("mensaje", validate);
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
					} catch (Exception ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}
	
	private JSONObject searchProductLdmJson(String idusuario, String idcliente, String path) {
		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						String tarifa = "1.VENTA";

						ArrayList<Producto> datosProductoLdm = new ArrayList<Producto>();

						datosProductoLdm = home.obtenerProductosLdm(idcliente, tarifa, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosProductoLdm.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("idproducto", datosProductoLdm.get(i).getIdProducto());
							map.put("nombre", datosProductoLdm.get(i).getNombre());

							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("productoldm", jsArray);
					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}
	
	private JSONObject buscarTercerosTextoJson(String idusuario, String idcliente, String dato, String path) {

		boolean continuar = true;

		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();

		try {
			if (idusuario.equals("")) {
				auxSeguridad.put("usuario", "No hay usuario"); // q tiene la variable usuario
				continuar = false;
			} else {
				auxSeguridad.put("usuario", idusuario);
			}

			auxRespuesta.put("usuario", idusuario);

			if (continuar) {
				Connection conHome = null;

				try {
					Conexion dataconexion = new Conexion(path);
					DataSource dataSourceOpenbravo = null;
					if (dataSourceOpenbravo == null) {
						dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
					}

					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);

					if (conHome == null) {

						auxRespuesta.put("respuesta",
								"No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");

					} else {

						ArrayList<Tercero> datosTercero = new ArrayList<Tercero>();

						datosTercero = home.buscarTercerosTexto(idcliente, dato, conHome);

						// poner la parte para convertir a json

						List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

						Map<String, Object> map;

						JSONArray jsArray = new JSONArray();

						for (int i = 0; i < datosTercero.size(); i++) {
							map = new HashMap<String, Object>();
							map.put("identificacion", datosTercero.get(i).getIdentificacion());
							map.put("nombre", datosTercero.get(i).getNombreComercial());
							list.add(map);
							jsArray.put(map);
						}

						auxRespuesta.put("tercerotexto", jsArray);

					}
				} catch (Exception ex) {
					log.warn(ex.getMessage());
				} finally {
					try {
						conHome.close();
						conHome = null;	
					} catch (SQLException ex) {
					}
				}
			} else {
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}

		return auxRespuesta;
	}
	
}
