package atrums.modelo;

public class Impuesto {
	private String idImpuesto;
	private String nombre;
	private String idCategoriaIva;
	
	public String getIdImpuesto() {
		return idImpuesto;
	}
	public void setIdImpuesto(String idImpuesto) {
		this.idImpuesto = idImpuesto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIdCategoriaIva() {
		return idCategoriaIva;
	}
	public void setIdCategoriaIva(String idCategoriaIva) {
		this.idCategoriaIva = idCategoriaIva;
	}
}