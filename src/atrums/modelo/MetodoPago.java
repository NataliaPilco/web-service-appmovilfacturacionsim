package atrums.modelo;

public class MetodoPago {
	private String idMetodoPago;
	private String nombre;	
	private double valorDefecto;	
	
	public String getIdMetodoPago() {
		return idMetodoPago;
	}
	public void setIdMetodoPago(String idMetodoPago) {
		this.idMetodoPago = idMetodoPago;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
	public void setValorDefecto(double valorDefecto) {
		this.valorDefecto = valorDefecto;
	}
	public double getValorDefecto() {
		return valorDefecto;
	}
}