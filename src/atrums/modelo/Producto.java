package atrums.modelo;

public class Producto {
	private String idProducto;
	private String fechaCreacion;
	private String identificador;
	private String nombre;
	private double precio;
	private String idCategoriaIva;
	private String nombreCategoriaIva;
	private double stock;  //stock en la tienda
	private double cantidadLdm;  //cantidad vendida --ldm
	private String tipo;

	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}	
	public String getIdCategoriaIva() {
		return idCategoriaIva;
	}
	public void setIdCategoriaIva(String idCategoriaIva) {
		this.idCategoriaIva = idCategoriaIva;
	}
	public String getNombreCategoriaIva() {
		return nombreCategoriaIva;
	}
	public void setNombreCategoriaIva(String nombreCategoriaIva) {
		this.nombreCategoriaIva = nombreCategoriaIva;
	}
	public double getStock() {
		return stock;
	}
	public void setStock(double stock) {
		this.stock = stock;
	}
	public double getCantidadLdm() {
		return cantidadLdm;
	}
	public void setCantidadLdm(double cantidadLdm) {
		this.cantidadLdm = cantidadLdm;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}