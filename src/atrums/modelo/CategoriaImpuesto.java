package atrums.modelo;

public class CategoriaImpuesto {
	private String idCategoriaImpuesto;
	private String nombre;	
	
	public String getIdCategoriaImpuesto() {
		return idCategoriaImpuesto;
	}
	public void setIdCategoriaImpuesto(String idCategoriaImpuesto) {
		this.idCategoriaImpuesto = idCategoriaImpuesto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}