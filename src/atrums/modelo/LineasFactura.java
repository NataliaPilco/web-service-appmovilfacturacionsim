package atrums.modelo;

public class LineasFactura {
	private String idFacturaLine;	
	private String idProducto;	
	private String nombre;
	private double cantidad;
	private double precioUnitario;
	private double valorTotal;
	private String idImpuesto;	
	private String impuesto;
	private double valorImpuesto;
	private String idFactura;
	//Producto idProducto = new Producto();
	
	public String getIdFacturaLine() {
		return idFacturaLine;
	}
	public void setIdFacturaLine(String idFacturaLine) {
		this.idFacturaLine = idFacturaLine;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}	
	public double getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}	
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getIdImpuesto() {
		return idImpuesto;
	}
	public void setIdImpuesto(String idImpuesto) {
		this.idImpuesto = idImpuesto;
	}
	public String getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}
	public double getValorImpuesto() {
		return valorImpuesto;
	}
	public void setValorImpuesto(double valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}
	public String getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}
}