package atrums.modelo;

public class Factura {
	
	private String idFactura;
	private String tipoDocumento;
	private String nroDocumento;
	private String puntoEmision;
	private String nroEstablecimiento;
	private String estado;
	private String idTercero;
	private String idCliente;
	private String identificacion;
	private String nombreComercial;
	private String razonSocial;
	private String tipoIdentificacion;
	private String email;
	private String nombre;
	private String apellido;
	private String tipoPersona;
	private String telefono;
	private String direccion;
	private String idUsuario;
	private String idDirecTercero;
	private String idModFactura;
	private String nroModDocumento;
	private String estadoSRI;
	private String fechaFactura;
	private double subTotal;
	private double total;
	
	public String getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getPuntoEmision() {
		return puntoEmision;
	}
	public void setPuntoEmision(String puntoEmision) {
		this.puntoEmision = puntoEmision;
	}
	public String getNroEstablecimiento() {
		return nroEstablecimiento;
	}
	public void setNroEstablecimiento(String nroEstablecimiento) {
		this.nroEstablecimiento = nroEstablecimiento;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIdTercero() {
		return idTercero;
	}
	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}	
	public String getIdDirecTercero() {
		return idDirecTercero;
	}
	public void setIdDirecTercero(String idDirecTercero) {
		this.idDirecTercero = idDirecTercero;
	}
	public String getIdModFactura() {
		return idModFactura;
	}
	public void setIdModFactura(String idModFactura) {
		this.idModFactura = idModFactura;
	}
	public String getNroModDocumento() {
		return nroModDocumento;
	}
	public void setNroModDocumento(String nroModDocumento) {
		this.nroModDocumento = nroModDocumento;
	}
	public String getEstadoSRI() {
		return estadoSRI;
	}
	public void setEstadoSRI(String estadoSRI) {
		this.estadoSRI = estadoSRI;
	}
	public String getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
}