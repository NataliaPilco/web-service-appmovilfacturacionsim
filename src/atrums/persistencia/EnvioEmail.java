package atrums.persistencia;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EnvioEmail {

	public boolean enviarMail(String contenido) {
		boolean respuesta = false;

		// El correo de env�o
		String usuarioEnvia = "1bbc580bbabe9c492281d1725590c8ae";
		String claveCorreo = "ef76c369582a709e439133e28ff1bf9f";
		String enviaCorreo = "facturacion@atrums.com";
		String host = "in-v3.mailjet.com";

		// La configuraci�n para enviar correo
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.user", usuarioEnvia);
		properties.put("mail.password", claveCorreo);

		// Obtener la sesion
		Session session = Session.getInstance(properties, null);

		try {
			// Crear el cuerpo del mensaje
			MimeMessage mimeMessage = new MimeMessage(session);

			// Agregar quien env�a el correo
			mimeMessage.setFrom(new InternetAddress(enviaCorreo, "Atrums It"));

			// Los destinatarios
			InternetAddress[] internetAddresses = { new InternetAddress("soporte@atrums.com"),
					new InternetAddress("info@atrums.com") };

			// Agregar los destinatarios al mensaje
			mimeMessage.setRecipients(Message.RecipientType.TO, internetAddresses);

			// Agregar el asunto al correo
			mimeMessage.setSubject("Nueva solicitud desde la aplicaci�n m�vil");

			// Creo la parte del mensaje
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(contenido);

			// Crear el multipart para agregar la parte del mensaje anterior
			MimeMultipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			// Agregar el multipart al cuerpo del mensaje
			mimeMessage.setContent(multipart);

			// Enviar el mensaje
			Transport transport = session.getTransport("smtp");
			transport.connect(usuarioEnvia, claveCorreo);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();

			respuesta = true;

		} catch (Exception ex) {
			ex.printStackTrace();
			respuesta = false;
		}
		return respuesta;
	}
}
