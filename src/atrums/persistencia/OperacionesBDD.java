package atrums.persistencia;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.CategoriaImpuesto;
import atrums.modelo.Factura;
import atrums.modelo.Impuesto;
import atrums.modelo.LineasFactura;
import atrums.modelo.Lista;
import atrums.modelo.MetodoPago;
import atrums.modelo.Producto;
import atrums.modelo.Tercero;
import atrums.util.Configuracion;

public class OperacionesBDD {
	static final Logger log = Logger.getLogger(OperacionesBDD.class);
	// private Configuracion configuracion = new Configuracion();

	public OperacionesBDD() {
		super();
	}

	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;

		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);

			if (connection.isClosed()) {
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}

		return connection;
	}

	public int loggin(String usuario, String password, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql, sql1;
		int loggin = 0;
		int registro = 0;
		int control = 0;
		int dato = 0;

		try {
			ResultSet rs = null, rs1 = null;
			statement = connection.createStatement();

			OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
			password = auxiliares.encryptSh1(password);

			sql = "SELECT count(*) as total, ad_user_id FROM ad_user ad " + "WHERE ad.username = '" + usuario
					+ "' AND ad.password = '" + password + "' AND ad.isactive = 'Y' group by 2;";

			rs = statement.executeQuery(sql);

			while (rs.next()) {
				if (rs.getInt("total") > 0) { // los datos son correctos
					// id = rs.getString("ad_user_id");
					control = 1;
				}
			}
			rs.close();

			if (control == 1) { // los datos son correctos
				// cierra todas las sessiones
				/*
				 * String sqlS; sqlS =
				 * "update ad_session_app set session_active = 'N', last_session_ping = now(), login_status = 'CNU' \r\n"
				 * +
				 * "		      where username = (select username from ad_user where ad_user_id = '"
				 * + id + "')\r\n" +
				 * "			  and server_url = 'app_movil' and session_active = 'Y' and login_status = 'S'"
				 * ;
				 * 
				 * statement.executeUpdate(sqlS); connection.commit();
				 */

				// verifica si hay una sesion activa
				sql = "select count(*) as session from ad_session_app where username = '" + usuario + "' "
						+ "and server_url = 'app_movil' and session_active = 'Y' and login_status = 'S';";

				rs1 = statement.executeQuery(sql);

				while (rs1.next()) {
					dato = rs1.getInt("session");
				}
				rs1.close();

				if (dato != 0) {
					loggin = 1; // ya esta conectado
				} else {

					sql1 = "INSERT INTO ad_session_app(ad_session_app_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, \r\n"
							+ "            websession, remote_addr, remote_host, processed, session_active, server_url, last_session_ping, username, login_status)\r\n"
							+ "    VALUES(get_uuid(), '0', '0', 'Y', now(), '0', now(), '0', \r\n"
							+ "            '', '0.0.0.0', '0.0.0.0', 'N', 'Y', 'app_movil', now(), '" + usuario
							+ "', 'S')";

					registro = statement.executeUpdate(sql1);

					if (registro == 1) {
						loggin = 2; // se creo una nueva conexion
						connection.commit();
					} else {
						loggin = 0;
						connection.rollback();
					}
				}

			} else {
				loggin = 0; // datos incorrectos
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}
		return loggin;
	}

	public ArrayList<Lista> listaClientesAtrums(String archivo) {
		ArrayList<Lista> lista = new ArrayList<Lista>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			String linea = br.readLine();

			String matriz[][] = new String[100][2];
			int contador = 0;

			while (linea != null) {
				String[] values = linea.split(",");
				Lista list = new Lista();
				for (int i = 0; i < values.length; i++) {
					matriz[contador][i] = values[i];
					if ((i % 2) == 0) {
						list.setArchivo(matriz[contador][i].toString());
					} else {
						list.setNombre(matriz[contador][i].toString());
					}
				} // lee la primera linea y le agrega al objeto
				lista.add(list);
				contador++;
				linea = br.readLine();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
			} catch (Exception e) {
			}
			;
		}
		return lista;
	}

	public String idUser(String idusuario, String password, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		String usuario = "";

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
			password = auxiliares.encryptSh1(password);

			sql = "SELECT ad.ad_user_id as idusuario " + "FROM ad_user ad " + "WHERE ad.username = '" + idusuario
					+ "' AND ad.password = '" + password + "' AND ad.isactive = 'Y';";
			rs = statement.executeQuery(sql);

			while (rs.next()) {
				usuario = rs.getString("idusuario");
			}
			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return usuario;
	}

	public String idClient(String idusuario, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		String idcliente = "";

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "SELECT ad.default_ad_client_id as idcliente " + "FROM ad_user ad " + "WHERE ad.ad_user_id = '"
					+ idusuario + "' AND ad.isactive = 'Y';";
			rs = statement.executeQuery(sql);

			while (rs.next()) {
				idcliente = rs.getString("idcliente");
			}
			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return idcliente;
	}

	public String registrarSolicitud(String nombre, String identificacion, String empresa, String telefono,
			String correo, String comentario_cliente, Connection connection) {
		Statement statement = null;
		int registro = 0;

		String mensaje = "";

		try {

			// envio de mail de solicitud
			boolean respuesta = false;
			EnvioEmail mail = new EnvioEmail();
			String contenido = "\nSe registr� un nueva solicitud de informaci�n\n" + "\nNombre: " + nombre
					+ "\nIdentificaci�n: " + identificacion + "\nEmpresa: " + empresa + "\nTel�fono / Celular: "
					+ telefono + "\nCorreo Electr�nico: " + correo + "\nMensaje: " + comentario_cliente;
			respuesta = mail.enviarMail(contenido);

			if (respuesta == true) {
				String sql;
				statement = connection.createStatement();
				sql = "INSERT INTO apm_solicitud "
						+ "			VALUES(get_uuid(), '0', '0', 'Y', now(), '100', now(), '100',"
						+ "					'" + nombre + "','" + identificacion + "','" + empresa + "','" + telefono
						+ "','" + correo + "','" + comentario_cliente + "',null,'CE')";

				registro = statement.executeUpdate(sql);

				if (registro == 1) {
					mensaje = "Su solicitud ha sido enviada exitosamente, pronto nuestro equipo de ventas se comunicara con usted.";
					connection.commit();
				} else {
					mensaje = "Error en el envi� de informaci�n";
					connection.rollback();
				}
			} else {
				mensaje = "Error en el envi� de informaci�n";
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			mensaje = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return mensaje;
	}

	public String insertarTercero(String idusuario, String idcliente, String identificacion, String nombre_comercial,
			String razon_social, String tipo_identificacion, String email, String nombre, String apellido,
			String tipo_persona, String telefono, String direccion, int control, Connection connection) {
		// el control es para saber si se inserto desde la transaccion o desde la
		// ventana cliente 1=cliente, 2=documento
		Statement statement = null;
		int registroP = 0;
		int registroU = 0;
		String idTercero = null;
		String idUser = null;
		String idLocation = null;
		String idPartnerLocation = null;

		String infor = "";
		String validar = null;

		try {
			String sql;
			String sqlP;
			String sqlI;
			ResultSet rs = null, rs1 = null;
			statement = connection.createStatement();

			sql = "select c_bpartner_id from c_bpartner where upper(value) like upper('" + identificacion
					+ "') and ad_client_id = '" + idcliente + "' limit 1;";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				validar = rs.getString("c_bpartner_id").toString();
			}
			rs.close();

			if (validar == null) {
				sqlI = "SELECT get_uuid() as c_bpartner_id, get_uuid() as ad_user_id, get_uuid() as c_location_id, get_uuid() as c_bpartner_location_id;";
				rs1 = statement.executeQuery(sqlI);
				while (rs1.next()) {
					idTercero = rs1.getString("c_bpartner_id").toString();
					idUser = rs1.getString("ad_user_id").toString();
					idLocation = rs1.getString("c_location_id").toString();
					idPartnerLocation = rs1.getString("c_bpartner_location_id").toString();
				}
				rs1.close();

				sqlP = "INSERT INTO c_bpartner(\r\n"
						+ "	c_bpartner_id, ad_client_id, ad_org_id, isactive, created, createdby, \r\n"
						+ "	updated, updatedby, value, name, name2, description, issummary, \r\n"
						+ "	c_bp_group_id, isonetime, isprospect, isvendor, iscustomer, isemployee, \r\n"
						+ "	issalesrep, referenceno, duns, url, ad_language, taxid, istaxexempt, \r\n"
						+ "	c_invoiceschedule_id, rating, salesvolume, numberemployees, naics, \r\n"
						+ "	firstsale, acqusitioncost, potentiallifetimevalue, actuallifetimevalue, \r\n"
						+ "	shareofcustomer, paymentrule, so_creditlimit, so_creditused, \r\n"
						+ "	c_paymentterm_id, m_pricelist_id, isdiscountprinted, so_description, \r\n"
						+ "	poreference, paymentrulepo, po_pricelist_id, po_paymentterm_id, \r\n"
						+ "	documentcopies, c_greeting_id, invoicerule, deliveryrule, deliveryviarule, \r\n"
						+ "	salesrep_id, bpartner_parent_id, socreditstatus, ad_forced_org_id, \r\n"
						+ "	showpriceinorder, invoicegrouping, fixmonthday, fixmonthday2, \r\n"
						+ "	fixmonthday3, isworker, upc, c_salary_category_id, invoice_printformat, \r\n"
						+ "	last_days, po_bankaccount_id, po_bp_taxcategory_id, po_fixmonthday, \r\n"
						+ "	po_fixmonthday2, po_fixmonthday3, so_bankaccount_id, so_bp_taxcategory_id, \r\n"
						+ "	fiscalcode, isofiscalcode, po_c_incoterms_id, so_c_incoterms_id, \r\n"
						+ "	fin_paymentmethod_id, po_paymentmethod_id, fin_financial_account_id, \r\n"
						+ "	po_financial_account_id, customer_blocking, vendor_blocking, \r\n"
						+ "	so_payment_blocking, po_payment_blocking, so_invoice_blocking, \r\n"
						+ "	po_invoice_blocking, so_order_blocking, po_order_blocking, so_goods_blocking, \r\n"
						+ "	po_goods_blocking, iscashvat, em_co_tipocontrib, em_co_tipo_identificacion, \r\n"
						+ "	em_co_bp_nro_aut_fc_sri, em_co_bp_fec_venct_aut_fc_sri, em_co_bp_nro_estab, \r\n"
						+ "	em_co_bp_punto_emision, em_co_bp_nro_aut_rt_sri, em_co_bp_fec_venct_aut_rt_sri, \r\n"
						+ "	em_co_email, em_co_niv_edu, em_co_vivienda, em_co_anios_res, \r\n"
						+ "	em_co_meses_res, em_co_total_meses, em_co_fechanac, em_co_ciudadnac, \r\n"
						+ "	em_co_nacionalidad, em_co_nombres, em_co_apellidos, em_co_com_tarj_cred, \r\n"
						+ "	em_co_natural_juridico, \r\n" + "	em_no_fechaingreso, \r\n"
						+ "	em_no_fechasalida, em_no_estadocivil, em_no_genero, em_no_fechanacimiento, \r\n"
						+ "	em_no_area_empresa_id, em_no_motivo_salida, em_no_sissalnet, \r\n"
						+ "	em_no_isdiscapacitado, em_no_istercera_edad) \r\n" + "	VALUES (\r\n" + "	'" + idTercero
						+ "', '" + idcliente + "', '0', 'Y', now(), '" + idusuario + "', \r\n" + "	now(), '"
						+ idusuario + "', '" + identificacion + "' , initcap('" + nombre_comercial + "'), initcap('"
						+ razon_social + "'), null, 'N', \r\n"
						+ "	(SELECT c_bp_group_id FROM c_bp_group WHERE ad_client_id = '" + idcliente
						+ "' limit 1), 'N', 'N', 'N', 'Y', 'N', \r\n" + "	'N', null, null, null, 'es_EC', '"
						+ identificacion + "', 'N', \r\n" + "	null, null, null, null, null, \r\n"
						+ "	null, '0', '0', '0', \r\n" + "	null, null, '0', '0', \r\n" + "	null, null, 'N', null, \r\n"
						+ "	null, null, null, null, \r\n" + "	null, null, 'I', null, null, \r\n"
						+ "	null, null, 'O', null, \r\n" + "	'Y', '000000000000000', null, null, \r\n"
						+ "	null, 'N', null, null, null, \r\n" + "	'1000', null, null, null, \r\n"
						+ "	null, null, null, null, \r\n" + "	null, null, null, null, \r\n"
						+ "	(select fin_paymentmethod_id from fin_paymentmethod where ad_client_id = '" + idcliente
						+ "' limit 1), null, null, \r\n" + "	null, 'N', 'N', \r\n" + "	'N', 'Y', 'Y', \r\n"
						+ "	'Y', 'Y', 'Y', 'Y', \r\n" + "	'N', 'N', null, '" + tipo_identificacion + "', \r\n"
						+ "	null, null, null, \r\n" + "	null, null, null, \r\n" + "	lower('" + email
						+ "'), null, null, null, \r\n" + "	null, null, null, null, \r\n" + "	null, initcap('"
						+ nombre + "'), initcap('" + apellido + "'), null, \r\n" + "	'" + tipo_persona + "', \r\n"
						+ "	null, \r\n" + "	null, null, null, null, \r\n" + "	null, null, null, \r\n" + "	'N', 'N')";

				registroP = statement.executeUpdate(sqlP);

				if (registroP == 1) {
					String sqlU;
					sqlU = "INSERT INTO ad_user(\r\n"
							+ "	ad_user_id, ad_client_id, ad_org_id, isactive, created, createdby, \r\n"
							+ "	updated, updatedby, name, description, password, email, supervisor_id, \r\n"
							+ "	c_bpartner_id, processing, emailuser, emailuserpw, c_bpartner_location_id, \r\n"
							+ "	c_greeting_id, title, comments, phone, phone2, fax, lastcontact, \r\n"
							+ "	lastresult, birthday, ad_orgtrx_id, firstname, lastname, username, \r\n"
							+ "	default_ad_client_id, default_ad_language, default_ad_org_id, \r\n"
							+ "	default_ad_role_id, default_m_warehouse_id, islocked, ad_image_id, \r\n"
							+ "	grant_portal_access, em_atecfe_check_email)\r\n" + "	VALUES (\r\n" + "	'" + idUser
							+ "', '" + idcliente + "', '0', 'Y', now(), '" + idusuario + "', \r\n" + "	now(), '"
							+ idusuario + "', initcap('" + nombre + "'), null, null, lower('" + email + "'), null, \r\n"
							+ "	'" + idTercero + "', 'N', null, null, null, \r\n" + "	null, null, null, '" + telefono
							+ "', null, null, null, \r\n" + "	null, null, null, initcap('" + nombre + "'), initcap('"
							+ apellido + "'), null, \r\n" + "	null, null, null, \r\n" + "	null, null, 'N', null, \r\n"
							+ "	'N', 'Y');";

					registroU = statement.executeUpdate(sqlU);

					sqlU = null;
					registroU = 0;
					sqlU = "INSERT INTO c_location(\r\n"
							+ "	c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, \r\n"
							+ "	updated, updatedby, address1, address2, city, postal, postal_add, \r\n"
							+ "	c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, \r\n"
							+ "	em_co_barrio) \r\n" + "	VALUES (\r\n" + "	'" + idLocation + "', '" + idcliente
							+ "', '0', 'Y', now(), '" + idusuario + "', \r\n" + "	now(), '" + idusuario
							+ "', initcap('" + direccion + "'), null, null, null, null, \r\n"
							+ "	(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, \r\n"
							+ "	null);";

					registroU = statement.executeUpdate(sqlU);

					sqlU = null;
					registroU = 0;
					sqlU = "INSERT INTO c_bpartner_location(\r\n"
							+ "	c_bpartner_location_id, ad_client_id, ad_org_id, isactive, created, \r\n"
							+ "	createdby, updated, updatedby, name, isbillto, isshipto, ispayfrom, \r\n"
							+ "	isremitto, phone, phone2, fax, c_salesregion_id, c_bpartner_id, \r\n"
							+ "	c_location_id, istaxlocation, upc, em_no_num_dir) \r\n" + "	VALUES (\r\n" + "	'"
							+ idPartnerLocation + "', '" + idcliente + "', '0', 'Y', now(), \r\n" + "	'" + idusuario
							+ "', now(), '" + idusuario + "', initcap('" + direccion + "'), 'Y', 'Y', 'Y', \r\n"
							+ "	'Y', null, null, null, null, '" + idTercero + "', \r\n" + "	'" + idLocation
							+ "', 'N', null, null);";

					registroU = statement.executeUpdate(sqlU);

					if (control == 1) { // viene de la ventana cliente
						infor = "El cliente se registr� correctamente";
					} else { // viene del documento
						infor = idTercero;
					}
					connection.commit();
				} else {
					infor = "Error, no se puede registrar el cliente";
				}
			} else {
				infor = "El nro. de identificaci�n ya se encuentra registrado";
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public ArrayList<Tercero> obtenerTercero(String idcliente, String identificacion, Connection connection) {

		ArrayList<Tercero> tercero = new ArrayList<Tercero>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select p.c_bpartner_id as idTercero, p.ad_client_id as idCliente, p.value as identificacion, p.name as nombreComercial, \r\n"
					+ "					p.name2 as razonSocial, p.em_co_tipo_identificacion as tipoIdentificacion, u.email as email, p.em_co_nombres as nombre,\r\n"
					+ "					p.em_co_apellidos as apellido, p.em_co_natural_juridico as tipoPersona, coalesce(u.phone, '0000000') as telefono, l.name as direccion, u.ad_user_id as idUsuario, l.c_bpartner_location_id as idDirecTercero\r\n"
					+ "					from c_bpartner as p \r\n"
					+ "					left join ad_user u on (p.c_bpartner_id = u.c_bpartner_id)\r\n"
					+ "					inner join c_bpartner_location l on (p.c_bpartner_id = l.c_bpartner_id)\r\n"
					+ "					where p.ad_client_id = '" + idcliente + "' and p.value = '" + identificacion
					+ "' and p.isactive = 'Y' and l.isactive = 'Y' limit 1;";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Tercero partner = new Tercero();
				partner.setIdTercero(rs.getString("idTercero").toString());
				partner.setIdCliente(rs.getString("idCliente").toString());
				partner.setIdentificacion(rs.getString("identificacion").toString());
				partner.setNombreComercial(rs.getString("nombreComercial").toString());
				partner.setRazonSocial(rs.getString("razonSocial").toString());
				partner.setTipoIdentificacion(rs.getString("tipoIdentificacion").toString());
				partner.setEmail(rs.getString("email").toString());
				partner.setNombre(rs.getString("nombre").toString());
				partner.setApellido(rs.getString("apellido").toString());
				partner.setTipoPersona(rs.getString("tipoPersona").toString());
				partner.setTelefono(rs.getString("telefono").toString());
				partner.setDireccion(rs.getString("direccion").toString());
				partner.setIdUsuario(rs.getString("idUsuario").toString());
				partner.setIdDirecTercero(rs.getString("idDirecTercero").toString());

				tercero.add(partner);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return tercero;
	}

	public ArrayList<Tercero> obtenerListaTerceros(String idcliente, Connection connection) {

		ArrayList<Tercero> tercero = new ArrayList<Tercero>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select *, coalesce((select phone from ad_user where c_bpartner_id = r.c_bpartner_id limit 1), '0000000') as telefono,\r\n"
					+ "       coalesce((select email from ad_user where c_bpartner_id = r.c_bpartner_id  and em_atecfe_check_email = 'Y' and isactive = 'Y' limit 1), '') as email  \r\n"
					+ "from (select p.c_bpartner_id, p.name as nombre_comercial,\r\n"
					+ "      (case when p.em_co_tipo_identificacion = '01' then 'RUC'\r\n"
					+ "            when p.em_co_tipo_identificacion = '02' then 'C�dula'\r\n"
					+ "            when p.em_co_tipo_identificacion = '03' then 'Pasaporte'\r\n"
					+ "            when p.em_co_tipo_identificacion = '07' then 'Consumidor Final' end) as tipo_identificacion,\r\n"
					+ "      (case when p.em_co_natural_juridico = 'PN' then 'Persona Natural'\r\n"
					+ "            when p.em_co_natural_juridico = 'PJ' then 'Persona Jur�dica' end) as tipo_persona,\r\n"
					+ "      p.value as identificacion\r\n" + "      from c_bpartner p \r\n"
					+ "      where p.iscustomer = 'Y' and p.ad_client_id = '" + idcliente + "' order by 2) as r";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Tercero partner = new Tercero();
				partner.setIdTercero(rs.getString("c_bpartner_id").toString());
				partner.setNombreComercial(rs.getString("nombre_comercial").toString());
				partner.setTipoIdentificacion(rs.getString("tipo_identificacion").toString());
				partner.setTipoPersona(rs.getString("tipo_persona").toString());
				partner.setIdentificacion(rs.getString("identificacion").toString());
				partner.setTelefono(rs.getString("telefono").toString());
				partner.setEmail(rs.getString("email").toString());

				tercero.add(partner);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return tercero;
	}

	public ArrayList<Producto> obtenerListaProductos(String idcliente, String tarifa, Connection connection) {

		ArrayList<Producto> producto = new ArrayList<Producto>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select p.m_product_id as idProducto, p.name as nombre, pp.pricestd as precio, \r\n"
					+ "       c.c_taxcategory_id as idCategoriaIva, c.name as nombreCategoriaIva\r\n"
					+ "		from m_product p\r\n"
					+ "		inner join c_taxcategory c on (p.c_taxcategory_id = c.c_taxcategory_id)\r\n"
					+ "		inner join m_productprice pp on (p.m_product_id = pp.m_product_id)\r\n"
					+ "		inner join m_pricelist_version v on (pp.m_pricelist_version_id = v.m_pricelist_version_id)\r\n"
					+ "		where p.ad_client_id = '" + idcliente + "' and p.issold = 'Y' and p.producttype in ('I','S') \r\n"
					+ "		and upper(v.name) like upper('" + tarifa
					+ "') and p.isactive = 'Y' and p.isbom = 'N' order by 2";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Producto product = new Producto();
				product.setIdProducto(rs.getString("idProducto").toString());
				product.setNombre(rs.getString("nombre").toString());
				product.setPrecio(rs.getDouble("precio"));
				product.setIdCategoriaIva(rs.getString("idCategoriaIva").toString());
				product.setNombreCategoriaIva(rs.getString("nombreCategoriaIva").toString());

				producto.add(product);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return producto;
	}

	public ArrayList<Impuesto> obtenerListaImpuesto(String idcliente, Connection connection) {

		ArrayList<Impuesto> impuesto = new ArrayList<Impuesto>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select x.c_tax_id as idImpuesto, x.ad_client_id as idCliente, x.name as nombre, c.c_taxcategory_id as idCategoriaIva\r\n"
					+ "       from c_tax x \r\n"
					+ "       inner join c_taxcategory c on (x.c_taxcategory_id = c.c_taxcategory_id)\r\n"
					+ "       where x.ad_client_id = '" + idcliente + "'\r\n"
					+ "       and x.isactive = 'Y' and x.sopotype in ('S','B') order by x.name";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Impuesto imp = new Impuesto();
				imp.setIdImpuesto(rs.getString("idImpuesto").toString());
				imp.setNombre(rs.getString("nombre").toString());
				imp.setIdCategoriaIva(rs.getString("idCategoriaIva").toString());

				impuesto.add(imp);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return impuesto;
	}
	
	public ArrayList<MetodoPago> obtenerListaMetodoPago(String idcliente, Connection connection) {

		ArrayList<MetodoPago> metodo = new ArrayList<MetodoPago>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select f.fin_paymentmethod_id as idmetodo_pago, f.name as nombre, (case when upper(f.name) = 'EFECTIVO' then 1 else 0 end) as valor_defecto\r\n" + 
					"       from FIN_FinAcc_PaymentMethod a inner join fin_paymentmethod f on (a.fin_paymentmethod_id = f.fin_paymentmethod_id)\r\n" + 
					"       where a.ad_client_id = '"+ idcliente +"' and a.payin_allow = 'Y' order by 2;";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				MetodoPago met = new MetodoPago();
				met.setIdMetodoPago(rs.getString("idmetodo_pago").toString());
				met.setNombre(rs.getString("nombre").toString());
				met.setValorDefecto(rs.getDouble("valor_defecto"));

				metodo.add(met);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return metodo;
	}

	public String buscarSucursal(String idcliente, String nombre, Connection connection) {
		Statement statement = null;
		String sql = null;
		String infor = "";

		try {

			ResultSet rs1 = null;
			statement = connection.createStatement();

			sql = "select ad_org_id from ad_org where upper(value) like upper('" + nombre + "') and ad_client_id = '"
					+ idcliente + "' and isactive = 'Y' order by created desc limit 1;";
			rs1 = statement.executeQuery(sql);
			while (rs1.next()) {
				infor = rs1.getString(1).toString();
			}

			rs1.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String buscarTipoDocumento(String idcliente, String nombre, Connection connection) {
		Statement statement = null;
		String sql = null;
		String infor = "";

		try {

			ResultSet rs1 = null;
			statement = connection.createStatement();

			sql = "select c_doctype_id from c_doctype where upper(name) like upper('" + nombre
					+ "') and ad_client_id = '" + idcliente + "' and isactive = 'Y' order by created desc limit 1;";
			rs1 = statement.executeQuery(sql);
			while (rs1.next()) {
				infor = rs1.getString(1).toString();
			}
			rs1.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String buscarCondicionPago(String idcliente, String nombre, Connection connection) {
		Statement statement = null;
		String sql = null;
		String infor = "";

		try {

			ResultSet rs1 = null;
			statement = connection.createStatement();

			sql = "select c_paymentterm_id from c_paymentterm where upper(name) like upper('" + nombre
					+ "') and ad_client_id = '" + idcliente + "' and isactive = 'Y' order by created desc limit 1;";
			rs1 = statement.executeQuery(sql);
			while (rs1.next()) {
				infor = rs1.getString(1).toString();
			}
			rs1.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String buscarTarifaCV(String idcliente, String nombre, Connection connection) {
		Statement statement = null;
		String sql = null;
		String infor = "";

		try {

			ResultSet rs1 = null;
			statement = connection.createStatement();

			sql = "select m_pricelist_id from m_pricelist where upper(name) like upper('" + nombre
					+ "') and ad_client_id = '" + idcliente + "' and isactive = 'Y' order by created desc limit 1;";
			rs1 = statement.executeQuery(sql);
			while (rs1.next()) {
				infor = rs1.getString(1).toString();
			}
			rs1.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String insertarInvoice(String idusuario, String idcliente, String idsucursal, String iddoctype,
			String idtercero, String iddireccion_tercero, String idcondicion_pago, String idtarifa, String idmodfactura,
			String idmetodo_pago, String pEmision, String nEstablecimiento, int tipo, Connection connection) {
		Statement statement = null;
		int registro = 0;
		String idInvoice = null;
		String documentno = null;
		String idsecuencial = null;
		// documentno = idusuario.substring(15);
		String sql = null;

		String infor = "";

		try {

			String sqlI = null;
			ResultSet rs1 = null;
			statement = connection.createStatement();

			sqlI = "SELECT get_uuid() as c_invoice_id;";
			rs1 = statement.executeQuery(sqlI);
			while (rs1.next()) {
				idInvoice = rs1.getString(1).toString();
			}
			rs1.close();

			sqlI = "select concat(r.prefijo, r.currentnext, r.sufijo) as valor, r.ad_sequence_id \r\n"
					+ "       from (select (to_char(b.currentnext)) as currentnext, b.prefix as prefijo, b.suffix as sufijo, b.ad_sequence_id\r\n"
					+ "       from c_doctype a, ad_sequence b\r\n"
					+ "       where a.docnosequence_id = b.ad_sequence_id\r\n" + "       and a.c_doctype_id = '"
					+ iddoctype + "') as r";
			rs1 = statement.executeQuery(sqlI);
			while (rs1.next()) {
				documentno = rs1.getString("valor").toString();
				idsecuencial = rs1.getString("ad_sequence_id").toString();
			}
			rs1.close();

			sql = "INSERT INTO c_invoice(\r\n"
					+ "       c_invoice_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, \r\n"
					+ "       issotrx, documentno, docstatus, docaction, processing, c_doctype_id, c_doctypetarget_id, description, \r\n"
					+ "       dateinvoiced, dateacct, c_bpartner_id, c_bpartner_location_id, isdiscountprinted, c_currency_id, \r\n"
					+ "       paymentrule, c_paymentterm_id, m_pricelist_id, createfrom, generateto, copyfrom, fin_paymentmethod_id, \r\n"
					+ "       em_aprm_processinvoice, em_co_nro_estab, em_co_punto_emision, em_atecfe_c_invoice_id, em_atimp_isproducto) \r\n"
					+ "	VALUES('" + idInvoice + "', '" + idcliente + "', '" + idsucursal + "', 'Y', now(), '"
					+ idusuario + "', now(), '" + idusuario + "', " + "'Y', '" + documentno
					+ "', 'DR', 'CO', 'N', '0', '" + iddoctype + "', null, " + "now(), date(now()), '" + idtercero
					+ "', '" + iddireccion_tercero + "', 'N', '100', " + "'P', '" + idcondicion_pago + "', '" + idtarifa
					+ "', 'N', 'N', 'N', '" + idmetodo_pago + "', " + "'CO', '" + nEstablecimiento + "', '" + pEmision
					+ "', '" + idmodfactura + "', 'N');";

			registro = statement.executeUpdate(sql);

			if (registro == 1) {
				// control de secuencial
				sqlI = "update ad_sequence set currentnext = currentnext + 1  where ad_sequence_id = '" + idsecuencial
						+ "'";
				statement.executeUpdate(sqlI);
				
				//actualizar session
				sqlI = "update ad_session_app set last_session_ping = now() where username = (select username \r\n" + 
						"from ad_user u where u.ad_user_id = '"+ idusuario +"') and session_active = 'Y' and login_status = 'S' ";
				statement.executeUpdate(sqlI);				

				infor = idInvoice;
				connection.commit();
			} else {
				infor = "Error";
				connection.rollback();
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Error" + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String insertarInvoiceLine(String idusuario, String idcliente, String iddoctype, String iddireccion_tercero,
			String identificacion, String nombre_comercial, String razon_social, String tipo_identificacion,
			String email, String nombre, String apellido, String tipo_persona, String telefono, String direccion,
			String idsucursal, String idtercero, String idproducto, double cantidad, double precio, double valor_total,
			String idimpuesto, String idfactura, String idmodfactura, String idcondicion_pago, String idtarifa,
			String idmetodo_pago, int tipo, int control_ldm, int control_tercero, Connection connection) {
		Statement statement = null;
		int registro = 0;
		double valorIva = 0.0;
		double valor_impuesto = 0.0;
		String idInvoiceline = null;
		String line = null;
		String value = null;

		String sql = null;

		String infor = "";

		try {

			String sqlI = null;
			ResultSet rs1 = null;
			statement = connection.createStatement();

			if (tipo == 1) {
				idmodfactura = "";
			}
			// verificar la existencia de la localidad del tercero
			String partner = null, partnerlocation = null;
			sqlI = "select p.c_bpartner_id, l.c_bpartner_location_id from c_bpartner p\r\n"
					+ "         inner join c_bpartner_location l on (p.c_bpartner_id = l.c_bpartner_id)\r\n"
					+ "         where p.c_bpartner_id = '" + idtercero + "';";
			rs1 = statement.executeQuery(sqlI);
			while (rs1.next()) {
				partner = rs1.getString("c_bpartner_id").toString();
				partnerlocation = rs1.getString("c_bpartner_location_id").toString();
			}
			rs1.close();

			if (idtercero.equals(null) || idtercero.equals("null")) { // usuario nuevo
				sqlI = "select c_bpartner_id from c_bpartner where value = '" + identificacion
						+ "' and ad_client_id = '" + idcliente + "' limit 1;";
				rs1 = statement.executeQuery(sqlI);
				while (rs1.next()) {
					value = rs1.getString("c_bpartner_id").toString();
				}
				rs1.close();
			}

			if (value == null) {
				if (partner == null) { // primero registra el tercero en caso de no existir

					idtercero = insertarTercero(idusuario, idcliente, identificacion, nombre_comercial, razon_social,
							tipo_identificacion, email, nombre, apellido, tipo_persona, telefono, direccion,
							control_tercero, connection);

					// busca la direccion del tercero para insertar en la factura
					sqlI = "select c_bpartner_location_id from c_bpartner_location where c_bpartner_id = '" + idtercero
							+ "';";
					rs1 = statement.executeQuery(sqlI);
					while (rs1.next()) {
						iddireccion_tercero = rs1.getString("c_bpartner_location_id").toString();
					}
					rs1.close();
				}

				if (!iddireccion_tercero.equals(null) || !iddireccion_tercero.equals("") || partnerlocation != null) {

					if (idfactura.equals(null) || idfactura.equals("") || idfactura.equals("null")) { // inserta la
																										// cabecera
																										// de la factura
																										// si
																										// no existe
						String pEmision = null, nEstablecimiento = null;
						sqlI = "select em_co_punto_emision, em_co_nro_estab from ad_org where ad_org_id = '"
								+ idsucursal + "' limit 1;";
						rs1 = statement.executeQuery(sqlI);
						while (rs1.next()) {
							pEmision = rs1.getString("em_co_punto_emision").toString();
							nEstablecimiento = rs1.getString("em_co_nro_estab").toString();
						}
						rs1.close();

						idfactura = insertarInvoice(idusuario, idcliente, idsucursal, iddoctype, idtercero,
								iddireccion_tercero, idcondicion_pago, idtarifa, idmodfactura, idmetodo_pago, pEmision,
								nEstablecimiento, tipo, connection);
					}

					// insertar lineas

					if (control_ldm == 1) { // si es ldm
						String resp = null;
						resp = searchProductLdm(idusuario, idcliente, idtercero, idproducto, idtarifa, idfactura,
								idsucursal, connection);
						if (resp == idfactura) {
							infor = idfactura; // "Se agrego el ldm a la factura";
							connection.commit();
						} else {
							infor = "Error";
							connection.rollback();
						}
					} else {

						sqlI = "select coalesce(rate,0) as valor from c_tax where c_tax_id = '" + idimpuesto + "'";
						rs1 = statement.executeQuery(sqlI);
						while (rs1.next()) {
							valorIva = rs1.getDouble("valor");
						}
						rs1.close();

						// buscar si existe el producto en la factura
						String c_invoice_line = null;
						double cantidad_inv = 0, total_inv = 0;
						sqlI = "select c_invoiceline_id, coalesce(sum(qtyinvoiced), 0) as qtyinvoiced from c_invoiceline where c_invoice_id = '"
								+ idfactura + "'\r\n" + "    and m_product_id = '" + idproducto
								+ "' group by 1 limit 1;";
						rs1 = statement.executeQuery(sqlI);
						while (rs1.next()) {
							c_invoice_line = rs1.getString("c_invoiceline_id");
							cantidad_inv = rs1.getDouble("qtyinvoiced");
						}
						rs1.close();

						if (cantidad_inv > 0) { // ya esta registrado el producto
							cantidad_inv = cantidad_inv + cantidad;
							total_inv = cantidad_inv * precio;
							valor_impuesto = total_inv * (valorIva / 100);

							String sqlS;
							sqlS = "update c_invoiceline set qtyinvoiced = '" + cantidad_inv + "', priceactual = '"
									+ precio + "', linenetamt = '" + total_inv + "',\r\n" + "    c_tax_id = '"
									+ idimpuesto + "', taxamt = '" + valor_impuesto + "', pricestd = '" + precio
									+ "',\r\n" + "    taxbaseamt = '" + total_inv + "' where m_product_id = '"
									+ idproducto + "' and c_invoiceline_id = '" + c_invoice_line + "'";

							registro = statement.executeUpdate(sqlS);
							
							if (registro > 0) {
								infor = idfactura; // "Se actualizo la linea
								connection.commit();
							} else {
								infor = "Error";
								connection.rollback();
							}

						} else {
							valor_impuesto = valor_total * (valorIva / 100);
							sqlI = "SELECT get_uuid() as c_invoiceline_id;";
							rs1 = statement.executeQuery(sqlI);
							while (rs1.next()) {
								idInvoiceline = rs1.getString(1).toString();
							}
							rs1.close();

							sqlI = "select coalesce(max(Line),0)+10 as line from c_invoiceline where c_invoice_id = '"
									+ idfactura + "';";
							rs1 = statement.executeQuery(sqlI);
							while (rs1.next()) {
								line = rs1.getString("line").toString();
							}
							rs1.close();

							sql = "INSERT INTO c_invoiceline("
									+ "c_invoiceline_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "
									+ "c_invoice_id, line, description, m_product_id, qtyinvoiced, priceactual, linenetamt, "
									+ "c_uom_id, c_tax_id, taxamt, pricestd, taxbaseamt, c_bpartner_id) \r\n"
									+ "	VALUES('" + idInvoiceline + "', '" + idcliente + "', '" + idsucursal
									+ "', 'Y', now(), '" + idusuario + "', now(), '" + idusuario + "', " + "'"
									+ idfactura + "', '" + line + "', null, '" + idproducto + "', '" + cantidad + "', '"
									+ precio + "', '" + valor_total + "', " + "'100', '" + idimpuesto + "', '"
									+ valor_impuesto + "', '" + precio + "', '" + valor_total + "', '" + idtercero
									+ "');";

							registro = statement.executeUpdate(sql);

							if (registro > 0) {
								infor = idfactura; // "Se agrego un producto a su factura";
								connection.commit();
							} else {
								infor = "Error";
								connection.rollback();
							}
						}
					}

				} else {
					infor = "Es necesario que el cliente tenga una direcci�n";
				}
			} else {
				infor = "El n�mero de identificaci�n ya se encuentra registrado";
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public ArrayList<Factura> obtenerInforFactura(String idfactura, Connection connection) {

		ArrayList<Factura> factura = new ArrayList<Factura>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select *, coalesce((select documentno from c_invoice where c_invoice_id = r.idmodfactura), 'null') as nroModDocumento\r\n"
					+ "from (select i.c_invoice_id as idFactura, d.name as tipoDocumneto, i.documentno as nroDocumentno, i.em_co_punto_emision as pEmision, i.em_co_nro_estab as nEstablecimiento, i.docstatus as estado, \r\n"
					+ "				 p.c_bpartner_id as idTercero, p.ad_client_id as idCliente, p.value as identificacion, p.name as nombreComercial,\r\n"
					+ "				 p.name2 as razonSocial, p.em_co_tipo_identificacion as tipoIdentificacion, u.email as email, p.em_co_nombres as nombre,\r\n"
					+ "				 p.em_co_apellidos as apellido, p.em_co_natural_juridico as tipoPersona, coalesce(u.phone, '0000000') as telefono, l.name as direccion, u.ad_user_id as idUsuario, l.c_bpartner_location_id as idDirecTercero,\r\n"
					+ "				 coalesce(i.em_atecfe_c_invoice_id, 'null') as idModFactura, round(i.totallines,2) as subTotal, round(i.grandtotal,2) as total\r\n"
					+ "				 from c_invoice i \r\n"
					+ "				 inner join c_doctype d on (i.c_doctypetarget_id = d.c_doctype_id)\r\n"
					+ "				 inner join c_bpartner p on (i.c_bpartner_id = p.c_bpartner_id)\r\n"
					+ "				 left join ad_user u on (p.c_bpartner_id = u.c_bpartner_id)\r\n"
					+ "				 inner join c_bpartner_location l on (p.c_bpartner_id = l.c_bpartner_id)\r\n"
					+ "			     where i.c_invoice_id = '" + idfactura + "') as r";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Factura invoice = new Factura();
				invoice.setIdFactura(rs.getString("idFactura").toString());
				invoice.setTipoDocumento(rs.getString("tipoDocumneto").toString());
				invoice.setNroDocumento(rs.getString("nroDocumentno").toString());
				invoice.setPuntoEmision(rs.getString("pEmision").toString());
				invoice.setNroEstablecimiento(rs.getString("nEstablecimiento").toString());
				invoice.setEstado(rs.getString("estado").toString());
				invoice.setIdTercero(rs.getString("idTercero").toString());
				invoice.setIdCliente(rs.getString("idCliente").toString());
				invoice.setIdentificacion(rs.getString("identificacion").toString());
				invoice.setNombreComercial(rs.getString("nombreComercial").toString());
				invoice.setRazonSocial(rs.getString("razonSocial").toString());
				invoice.setTipoIdentificacion(rs.getString("tipoIdentificacion").toString());
				invoice.setEmail(rs.getString("email").toString());
				invoice.setNombre(rs.getString("nombre").toString());
				invoice.setApellido(rs.getString("apellido").toString());
				invoice.setTipoPersona(rs.getString("tipoPersona").toString());
				invoice.setTelefono(rs.getString("telefono").toString());
				invoice.setDireccion(rs.getString("direccion").toString());
				invoice.setIdUsuario(rs.getString("idUsuario").toString());
				invoice.setIdDirecTercero(rs.getString("idDirecTercero").toString());
				invoice.setIdModFactura(rs.getString("idModFactura").toString());
				invoice.setNroModDocumento(rs.getString("nroModDocumento").toString());
				invoice.setSubTotal(rs.getDouble("subTotal"));
				invoice.setTotal(rs.getDouble("total"));

				factura.add(invoice);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return factura;
	}

	public ArrayList<LineasFactura> listarLineasFactura(String idfactura, Connection connection) {

		ArrayList<LineasFactura> linea = new ArrayList<LineasFactura>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "   select l.c_invoiceline_id as idFacturaLine, m.m_product_id as idProducto, m.name as nombre,\r\n"
					+ "          l.qtyinvoiced as cantidad, round(l.priceactual,2) as precioUnitario, round(l.linenetamt,2) as valorTotal, \r\n"
					+ "          t.name as impuesto, round(l.taxamt,2) as valorImpuesto, l.c_invoice_id as idFactura\r\n"
					+ "          from c_invoiceline l\r\n"
					+ "          inner join m_product m on (l.m_product_id = m.m_product_id)\r\n"
					+ "          left join c_tax t on (l.c_tax_id = t.c_tax_id)\r\n"
					+ "          where l.c_invoice_id = '" + idfactura + "' order by l.updated desc";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				LineasFactura line = new LineasFactura();
				line.setIdFacturaLine(rs.getString("idFacturaLine").toString());
				line.setIdProducto(rs.getString("idProducto").toString());
				line.setNombre(rs.getString("nombre").toString());
				line.setCantidad(rs.getDouble("cantidad"));
				line.setPrecioUnitario(rs.getDouble("precioUnitario"));
				line.setValorTotal(rs.getDouble("valorTotal"));
				line.setImpuesto(rs.getString("impuesto").toString());
				line.setValorImpuesto(rs.getDouble("valorImpuesto"));
				line.setIdFactura(rs.getString("idFactura").toString());

				linea.add(line);
			}

			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return linea;
	}

	public String verificarCreacionFactura(String idfactura, Connection connection) {
		Statement statement = null;
		String sql = null;
		String infor = "";

		try {

			ResultSet rs1 = null;
			statement = connection.createStatement();

			sql = "select c_invoice_id from c_invoice where c_invoice_id = '" + idfactura + "';";
			rs1 = statement.executeQuery(sql);
			while (rs1.next()) {
				infor = rs1.getString(1).toString();
			}
			rs1.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public String deleteInvoiceLine(String idline, Connection connection) {
		Statement statement = null;
		int registro = 0;

		String infor = "";

		try {

			String sql;
			statement = connection.createStatement();
			sql = "delete from c_invoiceline where c_invoiceline_id = '" + idline + "'";
			registro = statement.executeUpdate(sql);

			if (registro == 1) {
				infor = "Registro eliminado satisfactoriamente";
				connection.commit();
			} else {
				infor = "Error";
				connection.rollback();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public ArrayList<LineasFactura> selectInvoiceLine(String idline, Connection connection) {
		ArrayList<LineasFactura> linea = new ArrayList<LineasFactura>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select l.c_invoiceline_id as idFacturaLine, m.m_product_id as idProducto, m.name as nombre,\r\n"
					+ "          l.qtyinvoiced as cantidad, l.priceactual as precioUnitario, linenetamt as valorTotal, \r\n"
					+ "          t.c_tax_id as idImpuesto, t.name as impuesto, l.taxamt as valorImpuesto, l.c_invoice_id as idFactura\r\n"
					+ "          from c_invoiceline l\r\n"
					+ "          inner join m_product m on (l.m_product_id = m.m_product_id)\r\n"
					+ "          left join c_tax t on (l.c_tax_id = t.c_tax_id)\r\n"
					+ "          where l.c_invoiceline_id = '" + idline + "' order by l.created desc";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				LineasFactura line = new LineasFactura();
				line.setIdFacturaLine(rs.getString("idFacturaLine").toString());
				line.setIdProducto(rs.getString("idProducto").toString());
				line.setNombre(rs.getString("nombre").toString());
				line.setCantidad(rs.getDouble("cantidad"));
				line.setPrecioUnitario(rs.getDouble("precioUnitario"));
				line.setValorTotal(rs.getDouble("valorTotal"));
				line.setIdImpuesto(rs.getString("idImpuesto").toString());
				line.setImpuesto(rs.getString("impuesto").toString());
				line.setValorImpuesto(rs.getDouble("valorImpuesto"));
				line.setIdFactura(rs.getString("idFactura").toString());

				linea.add(line);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return linea;
	}

	public String updateInvoiceLine(String idline, String idproducto, String idimpuesto, double cantidad, double precio,
			double valor_total, Connection connection) {
		Statement statement = null;
		int registro = 0;
		double valor_impuesto = 0, valorIva = 0;

		String infor = "";

		try {

			String sql;
			statement = connection.createStatement();
			ResultSet rs = null;

			sql = "select coalesce(rate,0) as valor from c_tax where c_tax_id = '" + idimpuesto + "'";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				valorIva = rs.getDouble("valor");
			}
			rs.close();

			valor_impuesto = valor_total * (valorIva / 100);

			sql = "update c_invoiceline set qtyinvoiced = '" + cantidad + "', priceactual = '" + precio
					+ "', linenetamt = '" + valor_total + "', pricestd = '" + precio + "', taxbaseamt= '" + precio
					+ "', c_tax_id = '" + idimpuesto + "', taxamt = '" + valor_impuesto
					+ "', updated = now() where c_invoiceline_id = '" + idline + "'";
			registro = statement.executeUpdate(sql);

			if (registro == 1) {
				infor = "Registro actualizado correctamente";
				connection.commit();
			} else {
				infor = "Error";
				connection.rollback();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public Boolean existInvoiceLine(String idfactura, Connection connection) {
		Statement statement = null;
		int cont = 0;
		boolean exist = false;

		try {

			String sql1;
			statement = connection.createStatement();

			ResultSet rs1 = null;
			sql1 = "select count(c_invoiceline_id) from c_invoiceline where c_invoice_id = '" + idfactura + "';";
			rs1 = statement.executeQuery(sql1);
			while (rs1.next()) {
				cont = rs1.getInt(1);
			}
			rs1.close();

			if (cont != 0) {
				exist = true;
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return exist;
	}

	public String processInvoice(String idfactura, Connection connection) {
		Statement statement = null;
		int registro = 0;
		String infor = "";

		try {

			String sql;
			statement = connection.createStatement();

			sql = "update c_invoice set docstatus = 'CO', em_atecfe_docaction = 'PD', em_atecoff_docaction = 'PD', em_atecoff_docstatus = 'PD'\r\n"
					+ " where c_invoice_id = '" + idfactura + "'";
			registro = statement.executeUpdate(sql);

			if (registro == 1) {
				infor = "Documento procesado satisfactoriamente";
				connection.commit();
			} else {
				infor = "Error";
				connection.rollback();
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public ArrayList<Factura> listInvoice(String idcliente, String idsucursal, int tipo, Connection connection) {
		ArrayList<Factura> factura = new ArrayList<Factura>();

		Statement statement = null;
		String sql;
		String docbasetype = null;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			if (tipo == 1) {
				docbasetype = "ARI";
			} else {
				docbasetype = "ARC";
			}

			sql = "select *, coalesce((select documentno from c_invoice where c_invoice_id = r.idmodfactura), 'null') as nroModDocumento\r\n"
					+ "from (select i.c_invoice_id as idFactura, i.documentno as nroDocumento, (case when i.em_atecoff_docstatus = 'DR' then 'Borrador'\r\n"
					+ "										                                                   when i.em_atecoff_docstatus = 'PD' then 'Pendiente'\r\n"
					+ "										                                                   when i.em_atecoff_docstatus = 'AP' then 'Autorizado'\r\n"
					+ "										                                                   when i.em_atecoff_docstatus = 'RZ' then 'Rechazado'\r\n"
					+ "										                                                   when i.em_atecoff_docstatus = 'TR' then 'Tramitando'\r\n"
					+ "										                                                   when i.em_atecoff_docstatus = 'VO' then 'Anulado'\r\n"
					+ "										                                                   end) as estadoSRI, \r\n"
					+ "						to_char(date(i.dateinvoiced)) as fechaFactura, p.name as nombreComercial, p.value as identificacion, i.totallines as subtotal, i.grandtotal as total,\r\n"
					+ "						coalesce(i.em_atecfe_c_invoice_id, 'null') as idModFactura\r\n"
					+ "						from c_invoice i\r\n"
					+ "						inner join c_doctype d on (i.c_doctypetarget_id = d.c_doctype_id)\r\n"
					+ "						inner join c_bpartner p on (i.c_bpartner_id = p.c_bpartner_id)\r\n"
					+ "						where i.ad_client_id = '" + idcliente + "' and i.ad_org_id = '" + idsucursal
					+ "' \r\n" + "						and d.docbasetype = '" + docbasetype
					+ "' and d.em_atecfe_fac_elec = 'Y' and i.issotrx = 'Y'\r\n"
					+ "						order by i.created desc limit 25) as r";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Factura invoice = new Factura();
				invoice.setIdFactura(rs.getString("idFactura").toString());
				invoice.setNroDocumento(rs.getString("nroDocumento").toString());
				invoice.setEstadoSRI(rs.getString("estadoSRI").toString());
				invoice.setFechaFactura(rs.getString("fechaFactura").toString());
				invoice.setNombreComercial(rs.getString("nombreComercial").toString());
				invoice.setIdentificacion(rs.getString("identificacion").toString());
				invoice.setSubTotal(rs.getDouble("subtotal"));
				invoice.setTotal(rs.getDouble("total"));
				invoice.setIdModFactura(rs.getString("idModFactura").toString());
				invoice.setNroModDocumento(rs.getString("nroModDocumento").toString());

				factura.add(invoice);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return factura;
	}

	public ArrayList<CategoriaImpuesto> obtenerListaCategoriaImpuesto(String idcliente, Connection connection) {

		ArrayList<CategoriaImpuesto> impuesto = new ArrayList<CategoriaImpuesto>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select c_taxcategory_id as idCategoriaImpuesto, name as nombre\r\n"
					+ " from c_taxcategory c where c.ad_client_id = '" + idcliente
					+ "' and c.isactive = 'Y' order by name";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				CategoriaImpuesto imp = new CategoriaImpuesto();
				imp.setIdCategoriaImpuesto(rs.getString("idCategoriaImpuesto").toString());
				imp.setNombre(rs.getString("nombre").toString());

				impuesto.add(imp);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return impuesto;
	}

	public String insertProduct(String idusuario, String idcliente, String identificador, String nombre,
			String descripcion, String idimpuesto, String tipo, double precio, String tarifav, Connection connection) {
		// el control es para saber si se inserto desde la transaccion o desde la
		// ventana cliente 1=cliente, 2=documento
		Statement statement = null;
		int registroP = 0;
		int registroI = 0;
		String idProducto = null;
		String idTarifa = null;

		String infor = "";
		String validar = null;

		try {
			String sql;
			String sqlP;
			String sqlI;
			ResultSet rs = null, rs1 = null;
			statement = connection.createStatement();

			sql = "select m_product_id from m_product where upper(value) like upper('" + identificador + "') and ad_client_id = '"+ idcliente +"';";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				validar = rs.getString(1);
			}
			rs.close();

			if (validar == null) {
				sqlI = "SELECT get_uuid() as m_product_id";
				rs1 = statement.executeQuery(sqlI);
				while (rs1.next()) {
					idProducto = rs1.getString("m_product_id").toString();
				}
				rs1.close();

				sqlP = "INSERT INTO public.m_product(\r\n"
						+ "            m_product_id, ad_client_id, ad_org_id, created, createdby, updated, updatedby, value, name, \r\n"
						+ "            description, c_uom_id, salesrep_id, ispurchased, issold, isbom, \r\n"
						+ "            m_product_category_id, c_taxcategory_id, \r\n"
						+ "            producttype, m_locator_id, ispriceprinted, qtytype) \r\n" + "	 VALUES('"
						+ idProducto + "', '" + idcliente + "', '0', now(), '" + idusuario + "', now(), '" + idusuario
						+ "', '" + identificador + "', '" + nombre + "', \r\n" + "	        '" + descripcion
						+ "', '100', '" + idusuario + "', 'N', 'Y', 'N', \r\n"
						+ "	        (select m_product_category_id from m_product_category where ad_client_id = '"
						+ idcliente + "' and isactive = 'Y' order by created desc limit 1), '" + idimpuesto + "', \r\n"
						+ "	        '" + tipo + "', (select m_locator_id from m_locator where ad_client_id = '"
						+ idcliente + "' and isactive = 'Y' order by created desc limit 1), 'Y', 'N')";

				registroP = statement.executeUpdate(sqlP);

				if (registroP == 1) {
					ArrayList<CategoriaImpuesto> categoria = new ArrayList<CategoriaImpuesto>();

					sqlI = "select m_pricelist_version_id from m_pricelist_version \r\n" + 
							"      where isactive='Y' and upper(name) like upper('1.VENTA') and ad_client_id = '"+ idcliente +"'";
					rs1 = statement.executeQuery(sqlI);
					while (rs1.next()) {

						CategoriaImpuesto category = new CategoriaImpuesto();
						category.setIdCategoriaImpuesto(rs1.getString("m_pricelist_version_id").toString());

						categoria.add(category);
					}
					rs1.close();

					for (int i = 0; i < categoria.size(); i++) {
						idTarifa = categoria.get(i).getIdCategoriaImpuesto();

						sqlI = "INSERT INTO public.m_productprice(\r\n"
								+ "	 m_productprice_id, m_pricelist_version_id, m_product_id, ad_client_id, ad_org_id, \r\n"
								+ "	 created, createdby, updated, updatedby, pricestd)  \r\n" + "	 VALUES(get_uuid(), '"
								+ idTarifa + "', '" + idProducto + "', '" + idcliente + "', '0', \r\n" + "	 now(), '"
								+ idusuario + "', now(), '" + idusuario + "', '"+ precio +"')";

						registroI = statement.executeUpdate(sqlI);
					}

					if (registroI >= 1) {
						infor = "El producto se registr� correctamente";
					} else {
						infor = "Error, no se puede registra el producto";
					}

					connection.commit();
				} else {
					infor = "Error, no se puede registra el producto";
				}
			} else {
				infor = "El producto ya se encuentra registrado";
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			infor = "Inicio no registrado, error: " + ex.getMessage();
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return infor;
	}

	public ArrayList<Producto> listaProductos(String idcliente, String tarifa, Connection connection) {

		ArrayList<Producto> producto = new ArrayList<Producto>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select m.m_product_id as idProducto, to_char(m.created,'DD-MM-YYYY HH24:MI') as fechaCreacion, m.value as identificador, m.name as nombre, c.c_taxcategory_id as idCategoriaIva, c.name as nombreCategoriaIva, \r\n"
					+ "					       (case when m.producttype = 'I' then 'Art�culo'\r\n"
					+ "					             when m.producttype = 'S' then 'Servicio' end) as tipo, coalesce(sum(t.movementqty),0) as stock,\r\n"
					+ "					       coalesce ((select pp.pricestd from m_productprice pp\r\n"
					+ "                                                     inner join m_pricelist_version v on (pp.m_pricelist_version_id = v.m_pricelist_version_id)\r\n"
					+ "                                                     where pp.m_product_id = m.m_product_id and upper(v.name) like upper('"
					+ tarifa + "') limit 1),0) as precio\r\n" + "					       from m_product m \r\n"
					+ "					       left join m_transaction t on (m.m_product_id = t.m_product_id)\r\n"
					+ "					       inner join c_taxcategory c on (m.c_taxcategory_id = c.c_taxcategory_id)\r\n"
					+ "					       where m.isactive = 'Y' and m.ad_client_id = '" + idcliente + "' and m.issold = 'Y'\r\n"
					+ "					       group by 1,2,3,4,5,6 order by 4";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Producto product = new Producto();
				product.setIdProducto(rs.getString("idProducto").toString());
				product.setFechaCreacion(rs.getString("fechaCreacion").toString());
				product.setIdentificador(rs.getString("identificador").toString());
				product.setNombre(rs.getString("nombre").toString());
				product.setIdCategoriaIva(rs.getString("idCategoriaIva"));
				product.setNombreCategoriaIva(rs.getString("nombreCategoriaIva"));
				product.setTipo(rs.getString("tipo"));
				product.setStock(rs.getDouble("stock"));
				product.setPrecio(rs.getDouble("precio"));

				producto.add(product);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return producto;
	}

	public ArrayList<Factura> listInvoiceNC(String idcliente, String idsucursal, String idTercero,
			Connection connection) {
		ArrayList<Factura> factura = new ArrayList<Factura>();

		Statement statement = null;
		String sql;
		String docbasetype = "ARI";

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select i.c_invoice_id as idFactura, i.documentno as nroDocumento, i.grandtotal as total\r\n"
					+ "						from c_invoice i\r\n"
					+ "						inner join c_doctype d on (i.c_doctypetarget_id = d.c_doctype_id)\r\n"
					+ "						where i.ad_client_id = '" + idcliente + "' and i.ad_org_id = '" + idsucursal
					+ "' \r\n" + "						and d.docbasetype = '" + docbasetype
					+ "' and d.em_atecfe_fac_elec = 'Y' and i.issotrx = 'Y'\r\n"
					+ "					    and i.docstatus = 'CO' and i.isactive = 'Y'\r\n"
					+ "					    and i.c_bpartner_id = '" + idTercero + "'";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Factura invoice = new Factura();
				invoice.setIdFactura(rs.getString("idFactura").toString());
				invoice.setNroDocumento(rs.getString("nroDocumento").toString());
				invoice.setTotal(rs.getDouble("total"));

				factura.add(invoice);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return factura;
	}

	public ArrayList<Factura> obtenerDocModNC(String idfactura, Connection connection) {
		ArrayList<Factura> factura = new ArrayList<Factura>();

		Statement statement = null;
		String sql;
		String docbasetype = "ARI";

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select i.c_invoice_id as idFactura, i.documentno as nroDocumento, i.grandtotal as total\r\n"
					+ "						from c_invoice i\r\n"
					+ "						inner join c_doctype d on (i.c_doctypetarget_id = d.c_doctype_id)\r\n"
					+ "						where d.docbasetype = '" + docbasetype
					+ "' and d.em_atecfe_fac_elec = 'Y' and i.issotrx = 'Y'\r\n"
					+ "					    and i.docstatus = 'CO' and i.isactive = 'Y' and i.ispaid = 'N'\r\n"
					+ "					    and i.c_invoice_id = '" + idfactura + "'";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Factura invoice = new Factura();
				invoice.setIdFactura(rs.getString("idFactura").toString());
				invoice.setNroDocumento(rs.getString("nroDocumento").toString());
				invoice.setTotal(rs.getDouble("total"));

				factura.add(invoice);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return factura;
	}

	public boolean closeSession(String idusuario, String idsession, Connection connection) {
		Statement statement = null;
		int registro = 0;

		boolean close = false;

		try {
			// no se utiliza el adsession por que se pierde en memoria y me envia el
			// anterior
			String sql;
			statement = connection.createStatement();
			sql = " update ad_session_app set session_active = 'N', last_session_ping = now(), updated = now(), login_status = 'CNU'\r\n"
					+ "       where username like (select username from ad_user where ad_user_id = '" + idusuario
					+ "') and server_url = 'app_movil' and session_active = 'Y' and login_status = 'S' ";

			registro = statement.executeUpdate(sql);

			if (registro > 0) {
				close = true;
				connection.commit();
			} else {
				close = false;
				connection.rollback();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return close;
	}

	public boolean resetSession(String usuario, Connection connection) {
		Statement statement = null;
		int registro = 0;

		boolean close = false;

		try {

			String sql;
			statement = connection.createStatement();
			sql = " update ad_session_app set session_active = 'N', last_session_ping = now(), updated = now(), login_status = 'CNU'\r\n"
					+ "       where username like '" + usuario
					+ "' and server_url = 'app_movil' and session_active = 'Y' and login_status = 'S' ";

			registro = statement.executeUpdate(sql);

			if (registro > 0) {
				close = true;
				connection.commit();
			} else {
				close = false;
				connection.rollback();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return close;
	}

	public String checkSession(String idusuario, Connection connection) {
		Statement statement = null;
		String sql = null;
		String sesion = "";

		try {

			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select s.ad_session_app_id from ad_user u \r\n"
					+ "                     inner join ad_session_app s on (u.username = s.username)\r\n"
					+ "                     where u.ad_user_id = '" + idusuario + "'\r\n"
					+ "                     and s.server_url = 'app_movil' and s.session_active = 'Y' and s.login_status = 'S' \r\n"
					+ "                     limit 1";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				sesion = rs.getString("ad_session_app_id");
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return sesion;
	}

	public boolean verificarSession(String idsession, Connection connection) {
		Statement statement = null;
		String sql = null;
		boolean sesion = false;

		try {

			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select * from ad_session_app where ad_session_app_id = '" + idsession + "' "
					+ " and server_url = 'app_movil' and session_active = 'Y' and login_status = 'S'";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				sesion = true;
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return sesion;
	}

	public String searchProductLdm(String idusuario, String idcliente, String idtercero, String idproducto,
			String idtarifa, String idfactura, String idsucursal, Connection connection) {

		Statement statement = null;
		String sql = null, sqlI = null;
		String resp = "";
		String idInvoiceline = null;
		String line = null;
		String id_ldm = null, idcategoriaiva = null, idimpuesto = null;
		double cantidad = 0, precio = 0, valorIva = 0, valor_impuesto = 0, valor_total = 0, total_inv;
		int registro = 0;
		int cont = 0;

		ArrayList<Producto> producto = new ArrayList<Producto>();

		try {
			ResultSet rs = null, rs1 = null;
			statement = connection.createStatement();

			sql = "select pp.m_product_id as idProducto, pp.name as nombre, b.bomqty as cantidadLdm, pr.pricestd as precio, \r\n"
					+ "					       c.c_taxcategory_id as idCategoriaIva\r\n"
					+ "					       from m_product p\r\n"
					+ "					       inner join m_product_bom b on (p.m_product_id = b.m_product_id)\r\n"
					+ "					       inner join m_product pp on (b.m_productbom_id = pp.m_product_id)\r\n"
					+ "					       inner join c_taxcategory c on (pp.c_taxcategory_id = c.c_taxcategory_id)\r\n"
					+ "					       inner join m_productprice pr on (pp.m_product_id = pr.m_product_id)\r\n"
					+ "					       inner join m_pricelist_version v on (pr.m_pricelist_version_id = v.m_pricelist_version_id)\r\n"
					+ "					       where p.ad_client_id = '" + idcliente
					+ "' and p.issold = 'Y' and pp.producttype in ('I','S')\r\n" + "					       and p.m_product_id = '"
					+ idproducto + "' and p.isbom = 'Y'\r\n" + "					       and v.m_pricelist_id = '"
					+ idtarifa + "'\r\n"
					+ "					       and p.isactive = 'Y' and b.isactive = 'Y' and pp.isactive = 'Y'\r\n"
					+ "					       order by 2";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Producto product = new Producto();
				product.setIdProducto(rs.getString("idProducto").toString());
				product.setNombre(rs.getString("nombre").toString());
				product.setPrecio(rs.getDouble("precio"));
				product.setCantidadLdm(rs.getDouble("cantidadLdm"));
				product.setIdCategoriaIva(rs.getString("idCategoriaIva").toString());

				producto.add(product);
			}

			rs.close();

			for (int i = 0; i < producto.size(); i++) {

				idcategoriaiva = producto.get(i).getIdCategoriaIva();
				cantidad = producto.get(i).getCantidadLdm();
				id_ldm = producto.get(i).getIdProducto();
				precio = producto.get(i).getPrecio();
				valor_total = cantidad * precio;

				sqlI = "select c_tax_id as idimpuesto, coalesce(rate,0) as valor from c_tax where ad_client_id = '"
						+ idcliente + "'	\r\n" + "   and c_taxcategory_id = '" + idcategoriaiva
						+ "' and isactive = 'Y' limit 1;";
				rs1 = statement.executeQuery(sqlI);
				while (rs1.next()) {
					valorIva = rs1.getDouble("valor");
					idimpuesto = rs1.getString("idimpuesto");
				}
				rs1.close();

				// buscar si existe el producto en la factura
				String c_invoice_line = null;
				double cantidad_inv = 0;
				sqlI = "select c_invoiceline_id, coalesce(sum(qtyinvoiced), 0) as qtyinvoiced from c_invoiceline where c_invoice_id = '"
						+ idfactura + "'\r\n" + "    and m_product_id = '" + id_ldm + "' group by 1 limit 1;";
				rs1 = statement.executeQuery(sqlI);
				while (rs1.next()) {
					c_invoice_line = rs1.getString("c_invoiceline_id");
					cantidad_inv = rs1.getDouble("qtyinvoiced");
				}
				rs1.close();

				if (cantidad_inv > 0) { // ya esta registrado el producto
					cantidad_inv = cantidad_inv + cantidad;
					total_inv = cantidad_inv * precio;
					valor_impuesto = total_inv * (valorIva / 100);

					String sqlS;
					sqlS = "update c_invoiceline set updated = now(), qtyinvoiced = '" + cantidad_inv + "', priceactual = '" + precio
							+ "', linenetamt = '" + total_inv + "',\r\n" + "    c_tax_id = '" + idimpuesto
							+ "', taxamt = '" + valor_impuesto + "', pricestd = '" + precio + "',\r\n"
							+ "    taxbaseamt = '" + total_inv + "' where m_product_id = '" + id_ldm
							+ "' and c_invoiceline_id = '" + c_invoice_line + "'";

					registro = statement.executeUpdate(sqlS);
					connection.commit();
					cont = cont + registro;

				} else {
					valor_impuesto = valor_total * (valorIva / 100);
					sqlI = "SELECT get_uuid() as c_invoiceline_id;";
					rs1 = statement.executeQuery(sqlI);
					while (rs1.next()) {
						idInvoiceline = rs1.getString(1).toString();
					}
					rs1.close();

					sqlI = "select coalesce(max(Line),0)+10 as line from c_invoiceline where c_invoice_id = '"
							+ idfactura + "';";
					rs1 = statement.executeQuery(sqlI);
					while (rs1.next()) {
						line = rs1.getString("line").toString();
					}
					rs1.close();

					sql = "INSERT INTO c_invoiceline("
							+ "c_invoiceline_id, ad_client_id, ad_org_id, isactive, created, createdby, updated, updatedby, "
							+ "c_invoice_id, line, description, m_product_id, qtyinvoiced, priceactual, linenetamt, "
							+ "c_uom_id, c_tax_id, taxamt, pricestd, taxbaseamt, c_bpartner_id) \r\n" + "	VALUES('"
							+ idInvoiceline + "', '" + idcliente + "', '" + idsucursal + "', 'Y', now(), '" + idusuario
							+ "', now(), '" + idusuario + "', " + "'" + idfactura + "', '" + line + "', null, '"
							+ id_ldm + "', '" + cantidad + "', '" + precio + "', '" + valor_total + "', " + "'100', '"
							+ idimpuesto + "', '" + valor_impuesto + "', '" + precio + "', '" + valor_total + "', '"
							+ idtercero + "');";

					registro = statement.executeUpdate(sql);

					if (registro > 0) {
						connection.commit();
					} else {
						connection.rollback();
					}
					cont = cont + registro;
				}
			}

			if (cont > 0) {
				resp = idfactura; // "Se agrego un producto a su factura";
			} else {
				resp = "No se pudo insertar ningun producto";
				connection.rollback();
			}

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			try {
				connection.rollback();
			} catch (SQLException e) {
			}
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return resp;
	}
	
	public ArrayList<Producto> obtenerProductosLdm(String idcliente, String tarifa, Connection connection) {

		ArrayList<Producto> productoldm = new ArrayList<Producto>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select p.m_product_id as idProducto, p.name as nombre\r\n" + 
					"		from m_product p\r\n" + 
					"		inner join m_productprice pp on (p.m_product_id = pp.m_product_id)\r\n" + 
					"		inner join m_pricelist_version v on (pp.m_pricelist_version_id = v.m_pricelist_version_id)\r\n" + 
					"		where p.ad_client_id = '"+ idcliente +"' and p.issold = 'Y' and p.producttype in ('I','S') \r\n" + 
					"		and upper(v.name) like upper('"+ tarifa +"') and p.isverified = 'Y' and p.isactive = 'Y' and p.isbom = 'Y' order by 2";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Producto product = new Producto();
				product.setIdProducto(rs.getString("idProducto").toString());
				product.setNombre(rs.getString("nombre").toString());

				productoldm.add(product);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return productoldm;
	}
	
	public ArrayList<Tercero> buscarTercerosTexto(String idcliente, String dato, Connection connection) {

		ArrayList<Tercero> tercero = new ArrayList<Tercero>();

		Statement statement = null;
		String sql;

		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			sql = "select p.value as identificacion, p.name as nombreComercial\r\n" + 
					"       from c_bpartner p\r\n" + 
					"       where p.iscustomer = 'Y' and p.ad_client_id = '"+ idcliente +"' and p.isactive = 'Y'\r\n" + 
					"       and (upper(p.name) like upper('%"+ dato +"%') or upper(p.value) like upper('%"+ dato +"%')) order by 2";

			rs = statement.executeQuery(sql);

			while (rs.next()) {

				Tercero partner = new Tercero();
				partner.setIdentificacion(rs.getString("identificacion").toString());
				partner.setNombreComercial(rs.getString("nombreComercial").toString());

				tercero.add(partner);
			}

			rs.close();

		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (Exception e) {
			}
			;
		}

		return tercero;
	}

}
