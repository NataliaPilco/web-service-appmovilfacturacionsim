package atrums.persistencia;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validaciones {

	public String validarIdentificacion(String tipo_persona, String tipo_identificacion, String identificacion) {
		String mensaje = "";

		if (tipo_identificacion.equals("02") || tipo_identificacion.equals("01")) {

			try {
				int suma = 0;
				int residuo = 0;
				boolean validar = true;
				boolean privada = false;
				boolean publica = false;
				boolean natural = false;
				int identificacionProvincias = 24;
				int digitoVerificador = 0;
				int modulo = 11;

				int d1, d2, d3, d4, d5, d6, d7, d8, d9, d10;
				int p1, p2, p3, p4, p5, p6, p7, p8, p9;

				d1 = d2 = d3 = d4 = d5 = d6 = d7 = d8 = d9 = d10 = 0;
				p1 = p2 = p3 = p4 = p5 = p6 = p7 = p8 = p9 = 0;

				if (identificacion.length() == 0) {
					validar = false;
				} else if (tipo_identificacion.equals("02") && identificacion.length() != 10) {
					mensaje = " La CI de la persona natural es incorrecto";
					validar = false;
				} else if (tipo_identificacion.equals("01") && identificacion.length() != 13) {
					mensaje = " El RUC de la persona jur�dica es incorrecto";
					validar = false;
				}

				if (validar) {
					// Los primeros dos digitos corresponden al codigo de la
					// provincia
					int provincia = Integer.parseInt(identificacion.substring(0, 2));

					if (provincia <= 0 || provincia > identificacionProvincias) {
						mensaje = " CI/RUC no v�lido";
						validar = false;
						// JOptionPane
						// .showMessageDialog(Motor.getVentana(), "El c" + Motor.o
						// + "digo de la provincia (dos primeros d" + Motor.i +
						// "gitos) es inv" + Motor.a
						// + "lido");
					}

					if (validar) {
						// Aqui almacenamos los digitos de la cedula en variables.
						d1 = Integer.parseInt(identificacion.substring(0, 1));
						d2 = Integer.parseInt(identificacion.substring(1, 2));
						d3 = Integer.parseInt(identificacion.substring(2, 3));
						d4 = Integer.parseInt(identificacion.substring(3, 4));
						d5 = Integer.parseInt(identificacion.substring(4, 5));
						d6 = Integer.parseInt(identificacion.substring(5, 6));
						d7 = Integer.parseInt(identificacion.substring(6, 7));
						d8 = Integer.parseInt(identificacion.substring(7, 8));
						d9 = Integer.parseInt(identificacion.substring(8, 9));
						d10 = Integer.parseInt(identificacion.substring(9, 10));

						// El tercer digito es:
						// 9 para sociedades privadas y extranjeros
						// 6 para sociedades publicas
						// menor que 6 (0,1,2,3,4,5) para personas naturales
						if (d3 == 7 || d3 == 8) {
							mensaje = " CI/RUC no v�lido";
							// JOptionPane.showMessageDialog(Motor.getVentana(),
							// "El tercer d" + Motor.i
							// + "gito ingresado es inv" + Motor.a + "lido");
						}

						if (tipo_persona.equals("PJ") && tipo_identificacion.equals("01") && d3 < 6) {
							mensaje = " El RUC de la persona jur�dica es incorrecto";
						} else if (tipo_persona.equals("PN")
								&& (tipo_identificacion.equals("01") || tipo_identificacion.equals("02")) && d3 == 9) {
							mensaje = " CI/RUC no v�lido";
						}

						// Solo para personas naturales (modulo 10)
						if (d3 < 6) {
							natural = true;
							modulo = 10;
							p1 = d1 * 2;
							if (p1 >= 10)
								p1 -= 9;
							p2 = d2 * 1;
							if (p2 >= 10)
								p2 -= 9;
							p3 = d3 * 2;
							if (p3 >= 10)
								p3 -= 9;
							p4 = d4 * 1;
							if (p4 >= 10)
								p4 -= 9;
							p5 = d5 * 2;
							if (p5 >= 10)
								p5 -= 9;
							p6 = d6 * 1;
							if (p6 >= 10)
								p6 -= 9;
							p7 = d7 * 2;
							if (p7 >= 10)
								p7 -= 9;
							p8 = d8 * 1;
							if (p8 >= 10)
								p8 -= 9;
							p9 = d9 * 2;
							if (p9 >= 10)
								p9 -= 9;
						}

						// Solo para sociedades publicas (modulo 11)
						// Aqui el digito verficador esta en la posicion 9, en las otras
						// 2
						// en la pos. 10
						if (d3 == 6) {
							publica = true;
							p1 = d1 * 3;
							p2 = d2 * 2;
							p3 = d3 * 7;
							p4 = d4 * 6;
							p5 = d5 * 5;
							p6 = d6 * 4;
							p7 = d7 * 3;
							p8 = d8 * 2;
							p9 = 0;
						}

						/* Solo para entidades privadas (modulo 11) */
						if (d3 == 9) {
							privada = true;
							p1 = d1 * 4;
							p2 = d2 * 3;
							p3 = d3 * 2;
							p4 = d4 * 7;
							p5 = d5 * 6;
							p6 = d6 * 5;
							p7 = d7 * 4;
							p8 = d8 * 3;
							p9 = d9 * 2;
						}

						suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
						residuo = suma % modulo;

						// Si residuo=0, dig.ver.=0, caso contrario 10 - residuo
						digitoVerificador = residuo == 0 ? 0 : modulo - residuo;
						int longitud = identificacion.length(); // Longitud del string

						// ahora comparamos el elemento de la posicion 10 con el dig.
						// ver.

						if (publica == true) {
							if (digitoVerificador != d9) {
								mensaje = " El RUC de la empresa del sector p�blico es incorrecto";
								// JOptionPane.showMessageDialog(Motor.getVentana(),
								// "El ruc de la empresa del sector p"
								// + Motor.u + "blico es incorrecto.");
							}
							/*
							 * El ruc de las empresas del sector publico terminan con 0001
							 */
							if (!identificacion.substring(9, longitud).equals("0001")) {
								mensaje = " El RUC de la empresa del sector p�blico debe terminar con 0001";
								// JOptionPane.showMessageDialog(Motor.getVentana(),
								// "El ruc de la empresa del sector p"
								// + Motor.u + "blico debe terminar con 0001");
							}
							if (!tipo_identificacion.equals("01")) {
								mensaje = " Tipo identificaci�n incorrecto";
							}
						}

						if (privada == true) {
							if (digitoVerificador != d10) {
								mensaje = " El RUC de la persona jur�dica es incorrecto";
								// JOptionPane.showMessageDialog(Motor.getVentana(),
								// "El ruc de la persona jur�dica es incorrecto.");
							}
							if (!identificacion.substring(10, longitud).equals("001")) {
								mensaje = " El RUC de la persona jur�dica debe terminar con 001";
								// JOptionPane.showMessageDialog(Motor.getVentana(),
								// "El ruc de la persona jur�dica debe terminar con 001");
							}
							if (!tipo_identificacion.equals("01")) {
								mensaje = "Tipo identificaci�n incorrecto";
							}
						}

						if (natural == true) {
							if (digitoVerificador != d10) {
								mensaje = " La CI de la persona natural es incorrecto";
							}
							if (identificacion.length() > 10 && !identificacion.substring(10, longitud).equals("001")) {
								mensaje = " El RUC de la persona natural debe terminar con 001";
								// JOptionPane.showMessageDialog(Motor.getVentana(),
								// "El ruc de la persona natural debe terminar con 001");
							}

							if (mensaje.equals("")) {
								if (identificacion.length() > 10 && tipo_identificacion.equals("02")) {
									mensaje = " Tipo identificaci�n incorrecto";

								} else if (identificacion.length() == 10 && tipo_identificacion.equals("01")) {
									mensaje = " Tipo identificaci�n incorrecto";
								}
							}
						}
					}
				}

			} catch (Exception e) {

				mensaje = " CI/RUC no v�lido";
			}
		}
		return mensaje;
	}

	public String validarTelefono(String telefono) {
		String mensaje = "";
		int d1=-1, d2=-1, d3=-1, d4=-1, d5=-1, d6=-1, d7=-1, d8=-1, d9=-1, d10=-1;

		boolean validar = true;

		if (telefono.length() == 0) {
			validar = false;
		}

		if (validar) {

			if ((telefono.length() >= 7) && (telefono.length() <= 10)) {

				StringBuffer sb = new StringBuffer(telefono);
				for ( int i=telefono.length(); i < 10; i++) {
				  sb.append("0");
				}
				telefono = sb.toString();
				System.out.println(telefono);
				
				try {
					d1 = Integer.parseInt(telefono.substring(0, 1));
					d2 = Integer.parseInt(telefono.substring(1, 2));
					d3 = Integer.parseInt(telefono.substring(2, 3));
					d4 = Integer.parseInt(telefono.substring(3, 4));
					d5 = Integer.parseInt(telefono.substring(4, 5));
					d6 = Integer.parseInt(telefono.substring(5, 6));
					d7 = Integer.parseInt(telefono.substring(6, 7));
					d8 = Integer.parseInt(telefono.substring(7, 8));
					d9 = Integer.parseInt(telefono.substring(8, 9));
					d10 = Integer.parseInt(telefono.substring(9, 10));

					if ((d1 >= 0) && (d2 >= 0) && (d3 >= 0) && (d4 >= 0) && (d5 >= 0) && (d6 >= 0) && (d7 >= 0) && (d8 >= 0) && (d9 >= 0) && (d10 >= 0)) {
					} else {
						mensaje = "El n�mero de tel�fono es incorrecto";
					}
				} catch (Exception e) {
					// TODO: handle exception
					mensaje = "El n�mero de tel�fono es incorrecto";
				}
			} else {
				mensaje = "El n�mero de tel�fono debe tener entre 7 y 10 d�gitos num�ricos";
			}
		}

		return mensaje;
	}
	
	public String validarEmail(String email) {
		String mensaje = "";
		
		// Patr�n para validar el email
		Pattern pattern = Pattern
				.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		// El email a validar
		String validar = email;

		Matcher mather = pattern.matcher(validar);

		if (mather.find() == true) {
		} else {
			mensaje = "El email ingresado es inv�lido";
		}
		
		return mensaje;
	}
	
}
